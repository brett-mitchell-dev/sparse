
import { html as html, render } from '../../packages/@sprs/dom/index.mjs';
import { derive, signal } from '../../packages/@sprs/state/index.mjs';
import { rdom } from '../../sprs-ui-rct/index.mjs';

/*
 * const makeTextNode = value => document.createTextNode(value.toString());
 * const id = x => x;
 * class ListReactive extends Reactive {
 *   constructor(source, { key, create, update, delete: _delete } = {}) {
 *     super();
 *     this.cache = new Map();
 *     this.key = key || id;
 *     this.create = create || makeTextNode;
 *     this.update = update || null;
 *     this.delete = _delete || null;
 *     source.sub(this.callback);
 *     this.onValue(source.value);
 *   }
 * 
 *   onValue(newList) {
 *     const newListKeys = new Set();
 *     const newNodes = new Array(newList.length);
 *     let index = 0, len = newList.length;
 *     while(index < len) {
 *       const value = newList[index];
 * 
 *       const key = this.key(value, { index });
 *       newListKeys.add(key);
 *       if (this.cache.has(key)) {
 *         const node = this.cache.get(key);
 *         if (this.update) this.update(value, { key, node, index });
 *         newNodes[index] = node;
 *       }
 *       else {
 *         const newEntry = this.create(value, { key, index });
 *         this.cache.set(key, newEntry);
 *         newNodes[index] = newEntry;
 *       }
 * 
 *       index++;
 *     }
 * 
 *     super.onValue(newNodes);
 *   }
 * }
 */

const Item = ({ onDelete, text }) => {
  return html ('div', { style: 'display: flex; flex-direction: row' }, [
    html ('div', [rdom.value (text)]),
    html ('button', { on: { click: onDelete } }, ['Delete']),
  ]);
};

const Todo = () => {
  const todos = signal ([]);
  const input = signal ('');
  const count = signal (0);

  const addItem = () => {
    const newItemName = input.get ();
    if (newItemName.length > 0)
    {todos.mut (prev => [...prev, newItemName]);}
  };

  const deleteIndex = index => () => {
    todos.mut (todoArray => {
      todoArray.splice (index, 1);
      return todoArray;
    });
  };

  // Create a 10 item todo list
  todos.set (
    new Array (10)
      .fill (null)
      .map ((_, i) => `Item ${i}`),
  );

  /*
   * Render a list item for each one,
   * reacting to any changes in the list
   */
  const renderedTodos = 
    rdom.value (todos, todoList => todoList.map ((text, index) => Item ({
      text     : derive (count, c => text + c),
      onDelete : deleteIndex (index),
    })));
    /*
     * new ListReactive (
     *   todos,
     *   {
     *     key: text => text,
     * 
     *     create: (text, { index }) =>
     *       Item ({
     *         text     : count.derive (c => text + c),
     *         onDelete : deleteIndex (index),
     *       }),
     * 
     *     update: (text, { index, node }) => {},
     *   }
     * );
     */

  /*
   * Increment the counter (depended on
   * by each of the individual 1000 list
   * members)
   * NOTE: This does NOT re-render each list
   * item. It JUST updates the text within each
   * one
   * let i = 0;
   * const doit = () => {
   *   if (i >= 1_000) return;
   *   i++;
   *   count.set(i);
   *   setTimeout(doit, 0);
   * }
   * doit()
   */

  return [
    renderedTodos,
    html.input ({ listen: { change: (e) => input.set (e.target.value) } }),
    html.button ({ listen: { click: addItem } }, ['Add new item']),
  ];
};

const app = render (Todo ());

document.getElementById ('root').appendChild (app);
