
import { el, render } from '../../dom';
import { rdom } from '../../rdom';
import { signal } from '../../signal';

const Page = ({ header, paragraphs = ['DEFAULT TEXT'] }) => {
  const count = signal (0);
  const doubleCount = count.map (val => val * 2);

  // let i = 0;
  // const doit = () => {
  //   if (i >= 1_000) return;
  //   i++;
  //   count.set(i);
  //   setTimeout(doit, 0);
  // }
  // doit()

  return el.section ([
    // Static
    el.h1 ({ style: 'font-size: bold' }, [header]),

    // Reactive
    rdom.state (count, val => el.button (
      { listen: { click: () => count.set (val + 1) } },
      ['Clicked: ', val],
    )),

    el.p ([
      'Doubled count is: ',
      // Reactive
      rdom.state (doubleCount),
    ]),

    // Static
    paragraphs.map (
      paragraph => el.p ([paragraph]),
    ),
  ]);
};

const app = render (
  Page ({
    header     : 'My custom header',
    paragraphs : ['line 1', 'line 2'],
  }),
);

document.getElementById ('root').appendChild (app);
