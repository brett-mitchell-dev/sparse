
import { signal, join } from '../../signal/index.mjs';

const test = signal([]);
const accumulate = test.derive(
  (list, { previous }) => [...list, ...previous],
  { init: [] },
);
const count = signal(0);
join([count, test]).sub(value => console.log(value));;

accumulate.sub(value => console.log(value))

test.set([1])
test.set([2])
count.mut(n => n + 1);
test.set([3])
count.mut(n => n + 1);
count.mut(n => n + 1);

