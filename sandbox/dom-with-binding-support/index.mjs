import { html, render } from '../../dom/index.mjs';
import { derive, signal } from '../../state/index.mjs';

function Counter () {
  const count = signal (0);
  const inc = () => count.mut (c => c + 1);
  const dec = () => count.mut (c => c - 1);

  return [
    html ('span', [count]),
    html ('div', [
      html ('button', { on: { click: inc } }, ['Inc']),
      html ('button', { on: { click: dec } }, ['Dec']),
    ]),
  ];
}

function ColorPick () {
  const color = signal ('white');

  return [
    html (
      'select',
      {
        value : color,
        on    : { change: e => color.set (e.target.value) },
      },
      [
        html ('option', ['white']),
        html ('option', ['red']),
        html ('option', ['black']),
        html ('option', ['blue']),
        html ('option', ['cyan']),
        html ('option', ['grey']),
      ],
    ),

    html ('span', { style: { backgroundColor: color } }, [color]),
  ];
}

function ShowClick () {
  const button = html ('button', ['Click me!']);

  return [
    button,
    derive (button, 'Not fired yet...', {
      pointerdown : event => event.clientX,
      pointerup   : event => event.clientX,
    }),
  ];
}

document.getElementById ('root').appendChild (
  render ([
    html ('hr'),
    Counter (),

    html ('hr'),
    ColorPick (),

    html ('hr'),
    ShowClick (),
  ]),
);
