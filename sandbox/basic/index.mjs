import { draggable, dropzone } from '../../dnd/index.mjs';
import { applyListeners, html, render, setAttrs } from '../../dom/index.mjs';
import { rdom } from '../../rdom/index.mjs';
import { ListSignal } from '../../signal-std/index.mjs';
import { signal } from '../../signal/index.mjs';

function Counter () {
  const count = signal (0);
  const inc = () => count.mut (c => c + 1);
  const dec = () => count.mut (c => c - 1);

  return [
    // html ('span', [count]),
    html ('span', [rdom.value (count)]),
    html ('div', [
      html ('button', { listen: { click: inc } }, ['Inc']),
      html ('button', { listen: { click: dec } }, ['Dec']),
    ]),
  ];
}

/**
 * @template T
 * @typedef {import('../../signal/index.mjs').Signal <T>} Signal
 */

/**
 * @typedef {{
 *   text: Signal <string>,
 *   done: Signal <boolean>
 * }} TodoItem
 */

/** @type {TodoItem[]} */
const originalTodos = [
  { text: 'Take out the garbage', done: false },
  { text: 'Make dinner', done: false },
  { text: 'Go to the grocery store', done: false },
  { text: 'Get the cars oil changed', done: false },
  { text: 'Rake the yard', done: false },
  { text: 'Vacuum', done: false },
  { text: 'Dust the furniture', done: false },
].map (({ done, text }) => ({
  text : signal (text),
  done : signal (done),
}));

const DragListNativeApi = (() => {
  const showOverlay = signal (false);

  function AnnoyingPopup () {
    return html ('div', {
      style: {
        position        : 'absolute',
        backgroundColor : 'white',
        width           : '25%',
        height          : '100%',
        left            : '50%',
        transform       : 'translate(-50%, 0)',
      },
    }, ['Heres some text']);
  }

  /**
   * @param {TodoItem} todoItemData
   * @param {Signal <TodoItem>} currentDragTarget
   */
  function TodoItem (todoItemData, currentDragTarget) {
    const { done, text } = todoItemData;

    const todoItemDom = html ('li', { draggable: 'true' }, [
      rdom.value (done, v => html (
        'input',
        {
          type   : 'checkbox',
          ...(v && { checked: true }),
          listen : {
            change: ev => {
              return done.set (ev.target.checked);
            }, 
          },
        },
      )),

      rdom.value (text),
    ]);

    todoItemDom.addEventListener ('dragstart', (ev) => {
      ev.dataTransfer.effectAllowed = 'move';
      currentDragTarget.set (todoItemData);
    });

    return todoItemDom;
  }

  /**
   * @param {{
   *   currentDragTarget: Signal <TodoItem>,
   *   pull: ListSignal <TodoItem>,
   *   push: ListSignal <TodoItem> 
   * }} opts
   * @param {HTMLElement[]} children
   */
  function ListDragTarget ({ currentDragTarget, pull, push }, children) {
    const handleDrop = () => {
      if (currentDragTarget.value) {
        pull.mut (prev => prev.filter (todo => todo !== currentDragTarget.value));
        push.mut (prev => [...prev, currentDragTarget.value]);
        currentDragTarget.set (null);
      }
    };

    const ul = html (
      'ul',
      {
        style: {
          width           : '300px',
          height          : '300px',
          padding         : '15px 15px 15px 25px',
          marginRight     : '5px',
          backgroundColor : '#121413',
        }, 
      },
      children,
    );

    ul.addEventListener ('dragenter', ev => {
      ev.preventDefault ();
    });
    ul.addEventListener ('dragover', ev => {
      ev.preventDefault ();
    });
    ul.addEventListener ('drop', handleDrop);

    return ul;
  }

  function DragList () {
    const currentDragTarget = signal (null);

    /** @type {ListSignal <TodoItem>} */
    const list1 = new ListSignal (originalTodos);
    /** @type {ListSignal <TodoItem>} */
    const list2 = new ListSignal ([]);

    return [
      html (
        'div',
        {
          style: {
            position      : 'relative',
            display       : 'flex',
            flexDirection : 'row',
          }, 
        },

        [
          html (
            'button',
            { listen: { click: () => showOverlay.mut (prev => !prev) } },
            ['Toggle overlay'],
          ),

          ListDragTarget (
            {
              currentDragTarget,
              pull : list2,
              push : list1,
            },

            [rdom.value (
              list1,
              todos => todos.map (todo => TodoItem (todo, currentDragTarget)),
            )],
          ),

          ListDragTarget (
            {
              currentDragTarget,
              pull : list1,
              push : list2,
            },

            [rdom.value (
              list2,
              todos => todos.map (todo => TodoItem (todo, currentDragTarget)),
            )],
          ),

          rdom.value (showOverlay, value => value ? AnnoyingPopup () : null),
        ],
      ),
    ];
  }

  return DragList;
}) ();

const CustomDragApiExample = (() => {
  const droppedItems = signal ([]);

  function DragDemo () {
    const dragHandle = html (
      'div',
      {
        style: {
          width           : '15px',
          height          : '15px',
          margin          : 'auto',
          backgroundColor : 'black',
        }, 
      },
    );

    return html ('div', [
      html ('div', { style: { display: 'flex', flexDirection: 'row' } }, [
        html ('div', { style: { display: 'flex', flexDirection: 'column', width: '200px', height: '400px' } }, [
          draggable (
            html ('div', { style: { display: 'flex', flexDirection: 'column' } }, [
              dragHandle,
              html ('p', ['Drag me 1']),
              html ('p', ['Drag me 2']),
            ]),

            {
              dragRegion: dragHandle,

              on: {
                afterdragstart: ({ self }) => {
                  self.dragTarget.style.opacity = '0.5';
                },
              },
            },
          ),
        ]),

        html ('div', { style: { display: 'flex', flexDirection: 'column', width: '200px', height: '400px' } }, [
          dropzone (
            html (
              'div',
              { style: { width: '50px', height: '50px', backgroundColor: 'white' } },
            ),

            {
              on: {
                drop: (event) => {
                  droppedItems.mut (items => {
                    return [
                      ...items,
                      event.droppedDragAnchor.dragTarget.querySelector ('p').innerText,
                    ];
                  });
                },
                dragenter: (event) => {
                  event.self.element.style.backgroundColor = 'cyan';
                },
                dragleave: (event) => {
                  event.self.element.style.backgroundColor = 'white';
                },
              }, 
            },
          ),
        ]),

        html ('div', [
          html ('p', ['Put stuff here...']),

          rdom.value (droppedItems, items => items.map (item => html ('p', ["Here's an item " + item]))),
        ]),
      ]),
    ]);
  }

  return DragDemo;
}) ();

// const DragListIndividualList = (() => {}) ();

document.getElementById ('root').appendChild (
  render ([
    html ('details', [
      html ('summary', ['Counter']),
      Counter (),
    ]),

    html ('details', [
      html ('summary', ['Native API drag-and-drop todo list']),
      DragListNativeApi (),
    ]),

    html ('details', [
      html ('summary', ['Custom API drag-and-drop drag handle and drop zone']),
      CustomDragApiExample (),
    ]),
  ]),
);
