import { html, render } from '../../dom/index.mjs';
import { derive, signal } from '../../signal/index.mjs';

const sleep = ms => new Promise (rs => setTimeout (ms, rs));

const runThrowAwayDerivationsConfig = signal ({ iters: 10000, sleepMs: 0 });
function runThrowAwayDerivations () {
  const { iters, sleepMs } = runThrowAwayDerivationsConfig.get ();

  const parentSignal = signal (1);

  let i = 0;
  while (i < iters) {
    console.log (i);
    derive (parentSignal, value => value * 2);
    sleep (sleepMs);
    i++;
  }
}

document.getElementById ('root').appendChild (
  render ([
    html ('details', [
      html ('summary', ['Throw away derivations']),

      html ('label', { for: 'throw-away-derivations-iter-input' }, ['Iterations: ']),
      html (
        'input',
        {
          id     : 'throw-away-derivations-iter-input',
          value  : runThrowAwayDerivationsConfig.get ().iters,
          listen : {
            change: ev => runThrowAwayDerivationsConfig.mut (prev => ({
              ...prev,
              iters: Number (ev.target.value),
            })), 
          }, 
        },
      ),

      html (
        'button',
        { listen: { click: () => runThrowAwayDerivations () } },
        ['Run'],
      ),
    ]),
  ]),
);
