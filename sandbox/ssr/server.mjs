
// import { Page } from './ssr_components/Page.mjs';
import { PageWithHtml } from './ssr_components/PageWithEl.mjs';
import * as fs from 'fs/promises';
import * as http from 'http';
import * as path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath (import.meta.url);
const __dirname = path.dirname (__filename);

const port = process.env.PORT || 3000;

const read = f => fs.readFile (path.resolve (process.cwd (), ...f));

const exists = async f => {
  try {
    await fs.stat (f); return true;
  } catch (e) {
    console.log (e.message);
    return false;
  }
};

const getFileForUrl = async url => {
  const trimmed = url.split ('?')[0];
  if (trimmed.endsWith ('.mjs')) return trimmed;

  if (await exists (path.resolve (__dirname, '..', '..', trimmed.slice (1) + '.mjs'))) {
    return trimmed + '.js';
  }

  return trimmed + '/index.mjs';
};

const server = http.createServer (async (req, res) => {
  process.chdir (path.resolve (__dirname, '..', '..'));
  console.log (`Handling request: ${req.url}`);

  try {
    if (req.url.endsWith ('.html') || req.url === '/') {
      // const html = Page ();
      const html = PageWithHtml ();
      res.writeHead (200, { 'Content-Type': 'text/html' });
      res.end (html);
    }

    else {
      const resolvedFile = await getFileForUrl (req.url);
      const path = resolvedFile.split ('/').filter (segment => segment.length > 0);
      const src = await read (path);
      res.writeHead (200, { 'Content-Type': 'text/javascript' });
      res.end (src);
    }
  } catch (e) {
    res.writeHead (200, { 'Content-Type': 'text/javascript' });
    res.end (JSON.stringify ({ errorStack: e.stack }, null, 2));
  }
});

server.listen (port, () => console.log (`Listening on ${port}`));
