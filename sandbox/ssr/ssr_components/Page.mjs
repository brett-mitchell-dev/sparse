
const pretend_db_value = {
  /* eslint-disable */
  images: [
    'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.nairaland.com%2Fattachments%2F8686193_dylnw4twoaavj2x_jpeg39bd473c56b13b5c2fd245e26cbfc63e&f=1&nofb=1&ipt=6793ac2c421f29e23fad1d49264ca1755e7cd42814f31b45eac906435a139e70&ipo=images',
    'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fh6r1blM.jpg&f=1&nofb=1&ipt=ab9bb36a9636654dd20a73104be2fe575b22cf124ce9b85c248d1da7997ac77a&ipo=images',
    'https://www.designyourway.net/blog/wp-content/uploads/2018/01/wallpaper.wiki-Coolest-Desktop-Backgrounds-Pictures-HD-Free-PIC-WPD0010522.jpg',
  ],
  /* eslint-enable */

  numbers: [
    1,
    5,
    5000,
    -12,
  ],

  todos: [
    { text: 'Go to grocery store', done: false },
    { text: 'Cook enchiladas', done: false },
    { text: 'Vacuum the living room', done: true },
  ],
};

export const Page = () => {
  return `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="module" src="/doc/ssr/components/TodoItem.mjs"></script>
    <script type="module" src="/doc/ssr/components/TodoItem.mjs?tag-name=other-name"></script>
  </head>
  <body>

    <h1>
      This is a test page
    </h1>  

    <ul>
${pretend_db_value.todos.map (
    todo => `<todo-item text="${todo.text}" ${todo.done ? 'done' : ''}></todo-item>`,
  ).join ('\n')}
    </ul>

    <other-name text="testing" done></todo-item>

  </body>
</html>
  `;
};

/*

    <script src="/components/Spinner.js"></script>
    <script src="/components/ImageCarousel.js"></script>

<ol>
  ${pretend_db_value.numbers.map (num => `<spinner value="${num}"></spinner>`)}
</ol>

<carousel images="${JSON.stringify (pretend_db_value.images)}"></carousel>

*/
