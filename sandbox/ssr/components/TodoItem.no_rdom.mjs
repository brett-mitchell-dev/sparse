import { html } from '../../../dom/index.mjs';

export class TodoItem extends HTMLElement {

  connectedCallback () {
    this.done = this.getAttribute ('done') !== null;
    this.text = this.getAttribute ('text') || '';

    this.checkbox = html ('input', {
      type    : 'checkbox',
      checked : this.done ? '' : undefined,
      on      : { click: () => this.toggleDone () },
    });

    this.span = html ('span', [
      this.done ? this.text + ' Done!' : this.text,
    ]);

    this.li = html ('li', [
      this.checkbox,
      this.span,
    ]);

    this.replaceChildren (this.li);
  }

  toggleDone () {
    this.done = !this.done;
    if (this.done) {
      this.checkbox.setAttribute ('checked', '');
      this.span.innerText = this.text + ' Done!';
    } else {
      this.checkbox.removeAttribute ('checked');
      this.span.innerText = this.text;
    }
  }

}

TodoItem.define = (tagName = 'todo-item') => {
  customElements.define (tagName, TodoItem);
};

TodoItem.define ();
