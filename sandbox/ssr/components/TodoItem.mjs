
import { html } from '../../../dom/index.mjs';
import { rdom } from '../../../rdom/index.mjs';
import { signal } from '../../../signal/index.mjs';

export class TodoItem extends HTMLElement {

  connectedCallback () {
    this.done = signal (this.getAttribute ('done') !== null);
    this.text = this.getAttribute ('text') || '';

    this.replaceChildren (html ('li', [
      rdom.value (
        this.done,
        done => html ('input', {
          type    : 'checkbox',
          checked : done ? '' : undefined,
          on      : { click: () => this.done.mut (prev => !prev) },
        }),
      ),

      rdom.value (
        this.done,
        done => done ? this.text + ' Done!' : this.text,
      ),
    ]));
  }

}

// TODO: Figure out if there is a native way to define
//       these interfaces isomorphically. I'd like a
//       dev to be able to import from this file and use
//       TodoItem.define('tag-name') or use a tag:
//       <script type="module" src="/TodoList.mjs?tag-name=tag-name"

TodoItem.define = () => {
  customElements.define (
    new URL (import.meta.url)
      .searchParams
      .get ('tag-name')
    || 'todo-item',
    TodoItem,
  );
};

TodoItem.define ();
