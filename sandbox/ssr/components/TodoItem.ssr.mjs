
import { html } from '../../../ssr/index.mjs';

export class TodoItem {

  constructor ({ done, text }) {
    this.done = done || false;
    this.text = text || '';
  }

  render () {
    return (
      html ('todo-item', { done: this.done, text: this.text }, [
        html ('li', [
          html ('input', { type: 'checkbox', checked: this.done }),
          this.text,
        ]),
      ])
    );
  }

}
