//------- Current --------//

import { html } from '@sprs/dom';
import { rdom } from '@sprs/rdom';
import { unit } from '@sprs/state';

function Counter () {
  const count = unit (0);

  return [
    html ('label', { for: 'inc' }, ['Inc']),
    html ('button', { id: 'inc', listen: { click: () => count.mut (prev => prev + 1) } }),

    html ('label', { for: 'dec' }, ['Dec']),
    html ('button', { id: 'dec', listen: { click: () => count.mut (prev => prev - 1) } }),

    html ('span', ['Count: ', rdom.state (count, value => value)]),
    html ('span', ['Doubled Count: ', rdom.state (count, value => value * 2)]),
  ];
}
