//------- Option 1 --------//

import { html } from '@sprs/dom';
import { reactive } from '@sprs/reactive';
import { unit } from '@sprs/state';

function Counter () {
  const count = unit (0);
  const doubledCount = count.derive (value => value * 2);

  return reactive ([
    html ('label', { for: 'inc' }, ['Inc']),
    html ('button', { id: 'inc', listen: { click: () => count.set (count.get () + 1) } }),

    html ('label', { for: 'dec' }, ['Dec']),
    html ('button', { id: 'dec', listen: { click: () => count.mut (count.get () - 1) } }),

    html ('span', ['Count: ', count]),
    html ('span', ['Doubled Count: ', doubledCount]),
  ]);
}
