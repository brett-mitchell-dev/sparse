//------- Option 2 --------//

import { html } from '@sprs/dom';
import { r, reactive } from '@sprs/rdom';
import { unit } from '@sprs/state';

function Counter1 () {
  const count = unit (0);
  const doubledCount = count.derive (value => value * 2);

  return [
    html ('label', { for: 'inc' }, ['Inc']),
    html ('button', { id: 'inc', listen: { click: () => count.set (count.get () + 1) } }),

    html ('label', { for: 'dec' }, ['Dec']),
    html ('button', { id: 'dec', listen: { click: () => count.mut (count.get () - 1) } }),

    r (html) ('span', ['Count: ', count]),
    r (html) ('span', ['Doubled Count: ', doubledCount]),
  ];
}

// -- OR -- //

const rhtml = reactive (html);

function Counter2 () {
  const count = unit (0);
  const doubledCount = count.derive (value => value * 2);

  return [
    html ('label', { for: 'inc' }, ['Inc']),
    html ('button', { id: 'inc', listen: { click: () => count.set (count.get () + 1) } }),

    html ('label', { for: 'dec' }, ['Dec']),
    html ('button', { id: 'dec', listen: { click: () => count.mut (count.get () - 1) } }),

    rhtml ('span', ['Count: ', count]),
    rhtml ('span', ['Doubled Count: ', doubledCount]),
  ];
}

/*
 * What happens when you write something that just accepts values, but
 * then you want to pass in something thats reactive?
 */

function LabeledValue ({ label, value }) {
  return [
    html ('span', [label, value]),
  ];
}

function UsesIt () {
  const label = unit ('Label: ');
  const value = unit (1);

  return [
    LabeledValue ({ label: 'Static Label: ', value: 'Static Value' }),
    // This isn't going to work
    LabeledValue ({ label, value }),
  ];
}

/*
 * Conclusion: This turns reactivity into a leaky abstraction, and I'm not OK
 * with that.
 * Reactivity is going to have to be built into the @sprs/dom package natively
 */
