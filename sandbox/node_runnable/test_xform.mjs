import { Emitter } from '../../emitter/index.mjs';

class RequestEmitter extends Emitter {

  constructor (url, options) {
    super ({ events: ['init', 'response', 'error'] });
    this.url = url;
    this.options = options;
  }

  init () {
    this.emit ('init', {
      url     : this.url,
      options : this.options,
    });

    fetch (this.url, this.options)
      .then (response => {
        this.emit ('response', response);
      })
      .catch (error => {
        this.emit ('error', error);
      });
  }

}

const healthyLifeDecision = new RequestEmitter ('https://www.duckduckgo.com', { method: 'GET' });
healthyLifeDecision
  .xform ({ response: data => data.text () })
  .xform ({ response: text => text.slice (0, 100) })
  .on (
    'response',
    sliced => console.log (
      'Not sure why you needed this, but here are the first 100 characters of the Duck Duck Go home page HTML...',
      sliced,
    ),
  );

healthyLifeDecision.init ();
