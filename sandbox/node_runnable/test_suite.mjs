
import { test } from '../../test/index.mjs';

const mySuite = test (
  {
    title : 'My test suite',
    setup : async () => {
      const googlePageResponse = await fetch ('https://www.google.com');
      const googlePage = await googlePageResponse.text ();
      return { googlePage };
    },
  }, 

  context => [
    test ('Google page should have been loaded', (context) => {
      return typeof context.googlePage === 'string';
    }),

    test ({
      title : 'true should be false',
      setup : () => ({ somethingElse: 'a value' }),
    }, (context) => ({
      pass: true === false,

      reason: { page: context },
    })),
  ],
);

mySuite.run ();
