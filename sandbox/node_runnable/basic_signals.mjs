
import { Emitter } from '../../emitter/index.mjs';
import { derive, signal, xform } from '../../signal/index.mjs';

const emitter = new Emitter (['event_a', 'event_b']);

const counter = signal (1);
counter.on ('value', (value) => console.log (value));
const derivedSignal = derive (counter, counter.get (), { value: (prev) => prev });
const next = derive (derivedSignal, 'initial value', { value: (prev) => 'something' });

const transformed = xform (emitter, {
  event_a : (arg1) => `Hello ${arg1}`,
  event_b : () => 'world!',
});

transformed.on ('event_a', value => console.log (value));
transformed.on ('event', value => console.log (value));

counter.mut (x => x + 1);
emitter.emit ('event_a', 'John');
