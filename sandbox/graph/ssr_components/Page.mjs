const graph = {
  nodes: [
    { id: 1, label: 'a', x: 10,  y: 10 },
    { id: 2, label: 'b', x: 70,  y: 10 },
    { id: 3, label: 'c', x: 130, y: 10 },
    { id: 4, label: 'd', x: 10,  y: 70 },
    { id: 5, label: 'e', x: 70,  y: 70 },
    { id: 6, label: 'f', x: 130, y: 70 },
  ],
  edges: [
    { start: 1, end: 4, label: 'a -> d' },
  ],
};

export const Page = () => {
  return `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/doc/graph/components/Graph.css">
    <script type="module" src="/doc/graph/components/Graph.mjs"></script>
    <script type="module">
      import { Graph } from '/doc/graph/components/Graph.mjs';
      document.querySelector('#graph').appendChild(Graph({
        nodes: ${JSON.stringify (graph.nodes)},
        edges: ${JSON.stringify (graph.edges)},
      }));
    </script>
  </head>
  <body>

    <h1>
      Testing a graph
    </h1>  

    <div id="graph"></div>

  </body>
</html>
  `;
};
