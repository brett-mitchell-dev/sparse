import { debounceAnim } from '../../../anim/index.mjs';
import { getDragEvents } from '../../../dnd/index.mjs';
import { setAttrs, svg } from '../../../dom/index.mjs';
import { rdom } from '../../../rdom/index.mjs';

export const NODE_RADIUS = 25;

export const Node = ({ label, position }) => {
  const circle = svg ('circle', { fill: 'white', stroke: 'black', r: NODE_RADIUS });
  const text = svg ('text', { style: 'pointer-events: none', stroke: 'black' }, [
    rdom.value (label, labelVal => labelVal),
  ]);

  const dragEvents = getDragEvents (circle);
  dragEvents.on ('dragStart', () => setAttrs (circle, { fill: 'green' }));
  dragEvents.on ('dragEnd', () => setAttrs (circle, { fill: 'white' }));
  dragEvents.on (
    'dragMove',
    ({ dragOffset, event }) => {
      position.set ({
        x : event.offsetX + dragOffset.x,
        y : event.offsetY + dragOffset.y,
      });
    },
  );

  const move = debounceAnim (({ x, y }) => {
    setAttrs (circle, { cx: x + NODE_RADIUS, cy: y + NODE_RADIUS });
    setAttrs (text, { x: x + 10, y: y + 30 });
  });

  move (position.value);
  position.on ('value', ({ x, y }) => {
    move ({ x, y });
  });

  return ([circle, text]);
};
