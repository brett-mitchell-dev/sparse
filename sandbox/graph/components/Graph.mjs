
import { html, svg } from '../../../dom/index.mjs';
import { rdom } from '../../../rdom/index.mjs';
import { signal } from '../../../signal/index.mjs';
import { Edge } from './Edge.mjs';
import * as state from './Graph.state.mjs';
import { Node } from './Node.mjs';

const AddNodeForm = ({ graph, idIndex, show }) => {
  const label = signal ('');

  const addNode = () => {
    idIndex.mut (prev => prev + 1);
    graph.nodes.mut (prev => [...prev, state.Node ({
      id    : idIndex.value,
      label : label.value,
      x     : 10,
      y     : 10,
    })]);
    show.set (false);
  };

  const cancel = () => {
    show.set (false);
  };

  return html ('div', { class: 'new-item-form' }, [
    html ('h3', ['Add a new node']),
    html ('input', { on: { input: e => label.set (e.target.value) } }),
    html ('footer', [
      html ('button', { on: { click: addNode } }, ['Add']),
      html ('button', { on: { click: cancel } }, ['Cancel']),
    ]),
  ]);
};

const PickNode = ({ graph, selection }) => {
  return html ('span', [
    html (
      'select',
      { style: 'width: 50%', on: { input: e => selection.set (e.target.value) } },
      rdom.value (graph.nodes, nodes => nodes.map (node => html ('option', [node.id.value]))),
    ),

    // NOTE: This cheats a little bit... we need to join here to really be
    //       reactive. An update to the selected node name won't be reflected
    //       in the sidebar.
    html ('span', { style: 'width: 50%' }, [rdom.value (selection, selectedId => {
      const node = graph.nodes.value.find (({ id }) => id.value.toString () === selectedId);
      if (!node) return 'None';
      return node.label.value;
    })]),
  ]);
};

const AddEdgeForm = ({ graph, show }) => {
  const startNode = signal (null);
  const endNode = signal (null);
  const label = signal ('');

  const close = () => {
    show.set (false);
  };

  const addEdge = () => {
    graph.edges.mut (prev => [
      ...prev,
      state.Edge ({
        label : label.value,
        start : Number (startNode.value),
        end   : Number (endNode.value),
      }),
    ]);
    close ();
  };

  return html ('div', { class: 'new-item-form' }, [
    html ('h3', ['Add a new node']),
    html ('input', { on: { input: e => label.set (e.target.value) } }),

    PickNode ({ graph, selection: startNode }),
    PickNode ({ graph, selection: endNode }),

    html ('footer', [
      html ('button', { on: { click: addEdge } }, ['Add']),
      html ('button', { on: { click: close } }, ['Cancel']),
    ]),
  ]);
};

export const Graph = ({ edges, nodes }) => {
  const graph = state.Graph ({ edges, nodes });
  const idIndex = signal (Math.max (...nodes.map (({ id }) => id)));
  const showNodeForm = signal (false);
  const showEdgeForm = signal (false);

  const printGraph = () => {
    console.log (JSON.stringify (state.dehydrateGraph (graph), null, 2));
  };

  return html ('div', { class: 'graph-wrapper' }, [
    html ('div', { class: 'graph-sidebar' }, [
      html ('button', { on: { click: printGraph } }, ['Click to print graph']),

      html (
        'button',
        { on: { click: () => showNodeForm.mut (prev => !prev) } },
        [rdom.value (showNodeForm, show => show ? 'Cancel' : 'Click to add a node')],
      ),

      html (
        'button',
        { on: { click: () => showEdgeForm.mut (prev => !prev) } },
        [rdom.value (showEdgeForm, show => show ? 'Cancel' : 'Click to add an edge')],
      ),

      rdom.value (showNodeForm, show => show ? AddNodeForm ({ graph, show: showNodeForm, idIndex }) : null),
      rdom.value (showEdgeForm, show => show ? AddEdgeForm ({ graph, show: showEdgeForm }) : null),
    ]),

    svg (
      'svg',
      {
        version : '1.1',
        xmlns   : 'http://www.w3.org/2000/svg',
        width   : 500,
        height  : 500,
      },

      [
        rdom.value (
          graph.edges,
          edges => edges.map (
            ({ end, label, start }) => Edge ({
              start,
              end,
              label,
              nodes: graph.nodes,
            }),
          ),
        ),

        rdom.value (
          graph.nodes,
          (nodes) => nodes.map (Node),
        ),
      ],
    ),
  ]);
};
