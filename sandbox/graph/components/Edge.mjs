import { setAttrs, svg } from '../../../dom/index.mjs';
import { rdom } from '../../../rdom/index.mjs';
import { NODE_RADIUS } from './Node.mjs';

export const EdgeMask = () => (
  svg ('mask', { id: 'edge-mask' }, [
    svg ('rect', { x: 0, y: 0, width: 60, height: 60 }),
  ])
);

export const Edge = ({ end, label, nodes, start }) => {
  const startNode = nodes.value.find (node => node.id.value === start.value);
  const endNode = nodes.value.find (node => node.id.value === end.value);

  const line = svg ('line', { stroke: 'black' });
  const text = svg ('text', { style: 'pointer-events: none', stroke: 'black' }, [
    rdom.value (label, labelVal => labelVal),
  ]);

  const centerText = () => {
    const start = startNode.position.value;
    const end = endNode.position.value;
    setAttrs (text, {
      x: (
        start.x
        + NODE_RADIUS
        + 0.5 * (end.x - start.x)
      ),
      y: (
        start.y
        + NODE_RADIUS
        + 0.5 * (end.y - start.y)
      ),
    });
  };

  const moveStart = ({ x, y }) => {
    setAttrs (line, { x1: x + NODE_RADIUS, y1: y + NODE_RADIUS });
    centerText ();
  };

  const moveEnd = ({ x, y }) => {
    setAttrs (line, { x2: x + NODE_RADIUS, y2: y + NODE_RADIUS });
    centerText ();
  };

  moveStart (startNode.position.value);
  moveEnd (endNode.position.value);
  startNode.position.on ('value', moveStart);
  endNode.position.on ('value', moveEnd);

  return [line, text];
};
