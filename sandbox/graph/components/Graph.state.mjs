
import { signal } from '../../../signal/index.mjs';


export const Node = ({ id, label, x, y }) => {
  return {
    id       : signal (id),
    label    : signal (label),
    position : signal ({ x, y }),
  };
};
export const dehydrateNode = node => {
  return {
    id    : node.id.value,
    label : node.label.value,
    x     : node.position.value.x,
    y     : node.position.value.y,
  };
};


export const Edge = ({ end, label, start }) => {
  return {
    start : signal (start),
    end   : signal (end),
    label : signal (label),
  };
};
export const dehydrateEdge = edge => {
  return {
    start : edge.start.value,
    end   : edge.end.value,
    label : edge.label.value,
  };
};


export const Graph = ({ edges, nodes }) => {
  return {
    nodes : signal ((nodes || []).map (Node)),
    edges : signal ((edges || []).map (Edge)),
  };
};
export const dehydrateGraph = graph => {
  return {
    nodes : graph.nodes.value.map (dehydrateNode),
    edges : graph.edges.value.map (dehydrateEdge),
  };
};
