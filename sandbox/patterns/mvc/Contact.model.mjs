import { signal } from '../../../signal/index.mjs';

export const ContactModel = ({ email, firstname, lastname, middlename, phoneNumber } = {}) => {
  return {
    firstname   : signal (firstname || ''),
    lastname    : signal (lastname || ''),
    middlename  : signal (middlename || ''),
    email       : signal (email || ''),
    phoneNumber : signal (phoneNumber || ''),
  };
};
