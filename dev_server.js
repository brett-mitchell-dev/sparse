const http = require ('http');
const path = require ('path');
const fs = require ('fs/promises');
const port = process.env.PORT || 3000;

const exists = async f => fs.stat (f).then (() => true).catch (() => false);

const getContentType = url => {
  if (/\.m?js$/.test (url)) return 'text/javascript';
  if (/\.css$/.test (url)) return 'text/css';
  return 'text/html';
};

const walkDir = async (cb, dir = __dirname) => {
  const stats = await fs.readdir (dir);
  const proms = stats.map (async child => {
    const childDir = `${dir}/${child}`;
    const stats = await fs.stat (childDir);
    const reportDir = childDir.replace (`${__dirname}/`, '');
    const shouldContinue = await cb (reportDir, stats);
    if (shouldContinue && stats.isDirectory ()) {
      return walkDir (cb, childDir);
    }
  });
  return Promise.all (proms);
};

const STOP_AT = ['node_modules', '.git'];
const stop = (dir, stats) => stats.isDirectory () && STOP_AT.some (d => dir.endsWith (d));
const genLinks = async () => {
  let html = '<style>a { display: block; }</style>';
  await walkDir ((dir, stats) => {
    if (stats.isFile () && dir.endsWith ('.html')) {
      html += `<a href='${dir}'>${dir}</a>\n`;
    }
    return !stop (dir, stats);
  });
  return html;
};

const server = http.createServer (async (req, res) => {
  console.log (`Handling request: ${req.url}`);

  try {
    // Don't consider query parameters in route
    req.url = req.url.replace (/\?.*$/, '');
    if (req.url === '/favicon.ico') {
      return res.end ('');
    }

    if (req.url === '/') {
      res.writeHead (200, { 'Content-Type': 'text/html' });
      return res.end (await genLinks ());
    }

    const filepath = path.resolve (__dirname, `./${req.url}`);
    if (!await exists (filepath)) {
      res.writeHead (404);
      return res.end ();
    }

    const fileContents = await fs.readFile (filepath);
    res.writeHead (200, { 'Content-Type': getContentType (req.url) });
    return res.end (fileContents);
  } catch (e) {
    console.log (e);
    res.writeHead (200, { 'Content-Type': 'text/javascript' });
    return res.end (JSON.stringify ({ errorStack: e.stack }, null, 2));
  }
});

server.listen (port, () => console.log (`Listening on ${port}`));
