
export const debounceAnim = (cb) => {
  let rafId = null;
  return (...args) => {
    if (!rafId) {
      rafId = window.requestAnimationFrame (() => {
        cb.apply (null, args);
        rafId = null;
      });
    }
  };
};

