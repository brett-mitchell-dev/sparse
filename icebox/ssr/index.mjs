
const renderAttr = (attrName, attrValue) => {
  switch (typeof attrValue) {
  case 'undefined':
    return '';
  case 'string':
  case 'number':
    return `${attrName}="${attrValue}"`;
  case 'object':
    return `${attrName}="${JSON.stringify (attrValue)}"`;
  case 'boolean':
    return attrValue ? `${attrName}=""` : '';
  default:
    return '';
  }
};

export const render = (componentArray) => {
  if (!Array.isArray (componentArray)) {
    componentArray = [componentArray];
  }

  let rendered = '';

  let i = 0;
  while (i < componentArray.length) {
    const next = componentArray[i];
    i++;

    if (Array.isArray (next)) {
      componentArray.push.call (componentArray, next);
    }
    else if (next === null || typeof next === 'undefined') {
      rendered += Object.prototype.toString.call (next);
    }
    else {
      rendered += next.toString ();
    }
  }

  return rendered;
};

export const html = (tag, arg1, arg2) => {
  let opts = arg1, children = arg2;
  if (typeof arg2 === 'undefined') {
    if (Array.isArray (arg1)) {
      opts = {};
      children = arg1;
    } else {
      children = [];
    }
  }

  if (typeof opts !== 'object' || !opts) {
    opts = {};
  }

  const optNames = Object.keys (opts);
  let optIndex = 0, optLen = optNames.length, attributes = '';
  while (optIndex < optLen) {
    const optName = optNames[optIndex];
    if (optName !== 'on') {
      attributes += renderAttr (optName, opts[optName]);
    }
    optIndex++;
  }

  if (!Array.isArray (children)) {
    children = [children];
  }

  let childIndex = 0, childLen = children.length, renderedChildren = '';
  while (childIndex < childLen) {
    renderedChildren += render (children[childIndex]);
    childIndex++;
  }

  return `<${tag} ${attributes}>${renderedChildren}</${tag}>`;
};
