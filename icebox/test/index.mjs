const assert = (cond, msg) => { if (!cond) throw new Error (msg); };

const indent = (n, str) =>
  str.split ('\n')
    .map (line => ' '.repeat (n) + line)
    .join ('\n');

const getErrorString = error =>
  (error && error.stack)
    || error.message
    || error.toString ();

const defaultReporter = {
  onSuite: ({ nestLevel, title }) => {
    const output = `Suite - ${title}`;
    console.log (indent (nestLevel * 2, output));
  },

  onTestReport: ({ nestLevel, report, title }) => {
    const indentLevel = nestLevel * 2;

    if (report.error) {
      console.log (indent (indentLevel, '✗ Error encountered:'));
      console.log (indent (indentLevel + 2, getErrorString (report.error)));
    }

    else if (report.pass) {
      console.log (indent (indentLevel, `✓ ${title}`));
    }

    else {
      console.log (indent (indentLevel, `✗ ${title}`));
      if ('reason' in report) {
        const reason = typeof report.reason === 'string' ? report.reason : JSON.stringify (report.reason, null, 2);
        console.log (indent (indentLevel + 2, reason));
      }
    }
  },

  onSuiteFinish: ({ nestLevel, report }) => {
    const output = `Passing: ${report.passing} / ${report.total}`;
    console.log (indent ((nestLevel + 1) * 2, output));
  },
};

const isValidReporter = reporter => (
  reporter
  && typeof reporter.onSuite === 'function'
  && typeof reporter.onTestReport === 'function'
  && typeof reporter.onSuiteFinish === 'function'
);

const isValidTest = test => (
  test
  && typeof test === 'object'
  && typeof test.run === 'function'
);

const isValidTestImpl = impl => (
  typeof impl === 'function'
  || (
    Array.isArray (impl)
    && impl.every (inner => isValidTest (inner))
  )
);

export const test = (config, impl) => {
  if (typeof config === 'string') config = { title: config };

  assert (typeof config === 'object' && config !== null, "First positional parameter 'config' is required");
  assert (typeof config.title === 'string', "'config.title' is required");
  assert (!('setup' in config) || typeof config.setup === 'function', "'config.setup' must be a function");
  assert (!('teardown' in config) || typeof config.teardown === 'function', "'config.teardown' must be a function");
  assert (!('desc' in config) || typeof config.desc === 'string', "'config.teardown' must be a string");
  assert (!('reporter' in config) || isValidReporter (config.reporter), "'config.reporter' must be a valid reporter");
  assert (isValidTestImpl (impl), "'test' must be a function or an array of valid tests");

  const run = async (nestLevel = 0, parentContext = {}) => {
    const reporter = config.reporter || defaultReporter;
    const setup = config.setup || (() => parentContext);

    let report = null;
    let context = await setup ();

    try {
      let results = [];

      if (typeof impl === 'function') {
        impl = await impl (context);
      }

      if (Array.isArray (impl)) {
        results = new Array (impl.length);
        let i = 0, len = impl.length;
        while (i < len) {
          const childTest = impl[i];
          results[i] = await childTest.run (nestLevel + 1, context);
          i++;
        }
      }
      else {
        results = impl;
      }

      // Result is a suite of other tests
      if (Array.isArray (results)) {
        reporter.onSuite ({ ...config, nestLevel });

        const numPassing = results.filter (result => result.pass).length;
        report = {
          title: config.title,
          context,
          results,

          tests   : impl,
          total   : results.length,
          passing : numPassing,
          failing : results.length - numPassing,
        };

        reporter.onSuiteFinish ({ report, nestLevel });
      }

      else {
        if (results && typeof results === 'object' && typeof results.pass !== 'boolean') {
          throw new Error ("Invalid test output, must be boolean, or object with boolean property 'pass'");
        }

        report = {
          title: config.title,
          ...(typeof results === 'object' && results !== null ? results : { pass: results }),
        };
      }
    } catch (error) {
      report = {
        title   : config.title,
        message : 'Runtime error encountered',
        pass    : false,
        error,
      };
    }

    reporter.onTestReport ({ ...config, test: impl, report, nestLevel });

    if (config.teardown) await config.teardown (context);
    return report;
  };

  return { run };
};
