
// Summon an Eldrich horror from the Nether Abyss...
import * as hkt from '@sprs/ts-hkt';

// In all seriousness, partially applied higher-kinded
// types are required to get automatic type inference on
// nested contexts created by 'setup' functions in child
// suites.
//
// The lack of such a feature is one of my biggest gripes
// with Mocha.
//
// I wouldn't include something so awful otherwise.

interface Override extends hkt.tc <{ [key: string]: any }, { [key: string]: any }> {
  result: Omit<this['args'][0], keyof this['args'][1]> & this['args'][1]
}

interface Test <
  ParentContext extends { [key: string]: any } = {},
  Setup extends () => ({ [key: string]: any }),
  Body extends (
    | ((context: ReturnType <Setup> & ParentContext) => Result | Promise <Result>)
    | Test <ParentContext & ReturnType <Setup>> []
  )
> {
  title?: string;
  setup?: Setup;
  teardown?: (context: ParentContext & ReturnType <Setup>) => void;
  body: Body
}

// function test <ParentContext extends { [key: string]: any }, Context extends { [key: string]: any }> (opts: O <ParentContext, Context>, fn: (c: Context) => void);
// function test <ParentContext extends { [key: string]: any }, Context extends { [key: string]: any }, Children extends Test <Context> []> (opts: O <ParentContext, Context>, children: Children);

function test
  < ParentContext extends { [key: string]: any } = {},
    Setup extends () => ({ [key: string]: any }),
    Body extends (
      | ((context: ReturnType <Setup> & ParentContext) => Result | Promise <Result>)
      | Test <ParentContext & ReturnType <Setup>> []
    ) >
  (testDef: Test <ParentContext, Setup, Body>): Test <ParentContext, Setup, Body>;

const x = test({
  setup: () => ({ a: 123 }),
  body: ({ a }) => { a; },
});

const y = test({
  setup: () => ({ a: 123 }),
  body: [
    {
      setup: () => ({ b: 'a string' }),
      body: ({ b, a }) => {
        b
      },
    }
  ],
})

/*
export interface Report {
  title: string;
  context: { [key: string]: any };
  results: Result[];
  total: number;
  passing: number;
  failing: number;
}

export interface Reporter {
  onSuite(opts: { nestLevel: number, tests: Test[], title: string }): void;
  onTestResult(opts: { nestLevel: number, result: TestResult }): void;
  onReport(opts: { nestLevel: number, results: TestResult[] }): void;
}

export interface TestOpts <Context extends { [key: string]: any }, ParentContext extends { [key: string]: any } = {}> {
  title: string;
  setup? (): Context | Promise <Context>;
  teardown? (context: ParentContext & Context): void | Promise <void>;
  reporter?: Reporter;
}

export interface Test <Context extends { [key: string]: any }, ParentContext extends { [key: string]: any } = {}> {
  run(context?: Context & ParentContext, nestLevel?: number): Promise <Report>;
}

export interface TestResult {
  message?: string;
  pass: boolean;
  [key: string]: any;
}

export type TestFn <Context extends { [key: string]: any }> = (
  (context?: Context) => (
    | boolean
    | TestResult
    | Promise <boolean>
    | Promise <TestResult>
  )
)

export function test
  <
    Context extends { [key: string]: any } = {},
    ParentContext extends { [key: string]: any } = {},
  >
  (opts: string | TestOpts <Context>, tests: TestFn <Context & ParentContext> | Test <unknown, Context & ParentContext> [])
    : Test <Context>;
    */
