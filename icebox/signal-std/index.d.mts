
import { Emitter } from '@sprs/emitter'
import { Signal } from '@sprs/state'

export class ListSignal <T> extends Signal <T[]> {
  emitter: Emitter <['value', 'delete', 'insert', 'assign']>
  // emitter: Emitter <['value', 'setDiff', 'delete', 'insert', 'assign']>
  constructor (init?: T[]);

  on (
    event: 'value',
    listener: (
      value: T[],
      metadata: {
        previous: T,
        [key: string]: any;
      },
    ) => any,
  ): void;

  on (
    event: 'insert',
    listener: (
      value: T[],
      metadata: {
        insertStart: number
        inserted: T[],
        [key: string]: any;
      },
    ) => any,
  ): void;

  on (
    event: 'delete',
    listener: (
      value: T[],
      metadata: {
        deleted: number[],
        [key: string]: any;
      },
    ) => any,
  ): void;

  on (
    event: 'assign',
    listener: (
      value: T[],
      metadata: {
        setIndexes: [number, T][],
        [key: string]: any;
      },
    ) => any,
  ): void;

  // on (
  //   event: 'setDiff',
  //   listener: (
  //     value: T[],
  //     metadata: {
  //       previous: T,
  //       deleted: number[],
  //       inserted: number[],
  //       set: number[],
  //       moved: [number, number][],
  //       [key: string]: any;
  //     },
  //   ) => any,
  // ): void;

  // setDiff (value: T[], eq?: (v1: T, v2: T) => boolean): void;

  assign (index: number, value: T): void;
  assignAll (indexValuePairs: [number, T][]): void;
  assignRange (index: number, values: T[]): void;

  insert (index: number, value: T): void;
  // Note: This MUST treat all indexes as if they refer to the original array.
  // This is not a cumulative insert, and this does not use the indexes to designate
  // the FINAL location in the array.
  insertAll (indexValuePairs: [number, T][]): void;
  insertRange (index: number, values: T[]): void;

  delete (index: number): void;
  deleteAll (indexes: number[]): void;
  deleteRange (start: number, end?: number): void;

  splice (start: number, deleteCount?: number, ...items?: T[]): void;

  // Are these a good idea? How much parity do we want with the Array standard library?
  pop (): T;
  push (...values: T[]): number;
  shift (): T;
  unshift (...values: T[]): number;
  slice (start?: number, end?: number): ListSignal <T>;

  map <R> (
    create: (value: T, index: number) => R,
    dispose?: (existing: R, index: number) => void,
  ): ListSignal <R>;

  mapIndex <R> (
    create: (value: T, index: number) => R,
    dispose?: (existing: R, index: number) => void,
  ): ListSignal <R>;

  filter <R> (
    create: (value: T, index: number) => R,
    dispose?: (existing: R, index: number) => void,
  ): ListSignal <R>;

  filterIndex <R> (
    create: (value: T, index: number) => R,
    dispose?: (existing: R, index: number) => void,
  ): ListSignal <R>;
}
