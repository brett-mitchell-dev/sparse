//
// import { Signal } from '@sprs/state';
//
// // const eqByReference = (a, b) => a === b;
//
// /**
//  * A signal interface to a list of items.
//  *
//  * Allows granular reactions to individual list members,
//  * rather than requiring that an update to one member
//  * cause a reaction in all downstream dependencies.
//  *
//  * Note that the point of this interface is to have the
//  * developer state what they want updated.
//  *
//  * This may feel like more work up front, but more likely
//  * than not, explicitly stating what you want to happen
//  * to your list is a better experience than throwing a
//  * new value at a library. You _should_ be thinking about
//  * how your data structures change, and what that means
//  * for the rest of your application.
//  */
// class ListSignal extends Signal {
//
//   constructor (init) {
//     super (init);
//     this.emitter.events = [
//       'value',
//       'insert',
//       'delete',
//       'assign',
//       // 'setDiff',
//     ];
//   }
//
//   // setDiff (value, eq = eqByReference) {}
//
//   insert (index, value) {}
//   insertAll (index, values) {}
//
//   delete (start, end = start + 1) {
//     this.value.splice (start, end - start);
//     this.emitter.emit ('delete', this.value, { deleted: [] });
//   }
//
//   deleteAll (indexes) {
//     let i = 0,
//       newi = 0,
//       oldValue = this.value,
//       len = this.value.length;
//     const newValue = new Array (len - indexes.length);
//     while (i < len) {
//       if (!indexes.includes (i)) {
//         newValue[newi] = oldValue[i];
//         newi++;
//       }
//       i++;
//     }
//     this.value = newValue;
//     this.emitter.emit ('delete', newValue, { deleted: indexes });
//   }
//
//   deleteRange (start, end) {}
//
//   assign (index, value) {}
//   assignAll (indexValuePairs) {}
//
//   mapValue (create, dispose) {
//     const derived = new ListSignal (this.value);
//
//     this.on ('i');
//   }
//
//   mapIndex (create, dispose) {
//
//   }
//
// }
//
// class DictSignal extends Signal {
//
//   constructor (init) {
//     super (init);
//     this.emitter.events = ['value', 'set', 'insert', 'delete'];
//   }
//
//   setKey (key, value) {}
//   setAll (kvPairs) {}
//   deleteKey (key) {}
//   deleteAll (keys) {}
//
// }
//
// const l = new ListSignal ([
//   { id: 1, text: 'hi' },
//   { id: 2, text: 'there' },
//   { id: 3, text: 'friend' },
// ]);
//
// l.set (2, { id: 2, text: 'asdf' });
//
// export { DictSignal, ListSignal };
