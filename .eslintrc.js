const OFF   = 0;
const WARN  = 1;
const ERROR = 2;

module.exports = {
  extends : ['eslint:recommended'],
  plugins : ['sort-destructure-keys', 'perfectionist'],
  env     : { browser: true, es2021: true },

  overrides: [
    {
      files: ["./**/*.test.mjs", "./**/*.test.js"],
      env: {
        browser : false,
        es2021  : true,
        mocha   : true,
        node    : true,
      }
    }
  ],

  ignorePatterns: [
    '*.d.ts',
    '*.d.mts',
    '*.ts',
    '.eslintrc.js',
    'packages/**/dist/**/*',
  ],

  parserOptions: {
    ecmaVersion : 'latest',
    sourceType  : 'module',
  },

  rules: {
    'sort-destructure-keys/sort-destructure-keys': ERROR,

    'perfectionist/sort-exports'       : ERROR,
    'perfectionist/sort-imports'       : [
      ERROR,
      { groups: [
        'type',
        ['builtin', 'external'],
        'internal-type',
        'internal',
        ['parent-type', 'sibling-type', 'index-type'],
        ['parent', 'sibling', 'index'],
        'object',
        'unknown',
      ] },
    ],

    'perfectionist/sort-named-exports' : ERROR,
    'perfectionist/sort-named-imports' : ERROR,

    'curly'                    : [ERROR, 'multi-line'],
    'func-call-spacing'        : [ERROR, 'always'],
    'implicit-arrow-linebreak' : OFF,

    'max-len': [ERROR, {
      code     : 120,
      comments : 80,
    }],

    'no-multi-spaces': [ERROR, {
      exceptions: {
        Property           : true,
        VariableDeclarator : true,
        ImportDeclaration  : true,
      },
    }],

    'no-multiple-empty-lines': [ERROR, {
      max    : 2,
      maxBOF : 1,
    }],

    'import/prefer-default-export' : OFF,
    'key-spacing'                  : [ERROR, {
      align: {
        afterColon  : true,
        beforeColon : true,
      },
      multiLine: {
        afterColon  : true,
        beforeColon : false,
      },
    }],

    'lines-between-class-members'      : [ERROR, 'always', { exceptAfterSingleLine: true }],
    'no-confusing-arrow'               : OFF,
    'no-nested-ternary'                : ERROR,
    'no-plusplus'                      : OFF,
    'no-underscore-dangle'             : OFF,
    'no-unexpected-multiline'          : OFF,
    'no-use-before-define'             : OFF,
    'nonblock-statement-body-position' : OFF,
    'object-curly-newline'             : [ERROR, { multiline: true }],
    'operator-linebreak'               : [ERROR, 'before', {
      overrides: {
        '='  : 'after',
        '=>' : 'after',
      },
    }],

    'array-bracket-spacing' : [ERROR, 'never'],
    'object-curly-spacing'  : [ERROR, 'always'],

    'padded-blocks'               : [ERROR, { classes: 'always' }],
    'quote-props'                 : [ERROR, 'consistent-as-needed'],
    'space-before-function-paren' : [ERROR, 'always'],
    'space-in-parens'             : [ERROR, 'never'],

    'indent'          : [ERROR, 2],
    'linebreak-style' : [ERROR, 'unix'],
    'quotes'          : [ERROR, 'single', { avoidEscape: true }],
    'semi'            : [ERROR, 'always'],
    'comma-dangle'    : [ERROR, 'always-multiline'],

    'multiline-comment-style': ERROR,
  },
};
