
export class Emitter {

  constructor (events) {
    this.listeners = {};
    this.events = events || [];

    if (!Array.isArray (this.events)) {
      throw new Error ("Parameter 'events' must be an array");
    }

    let i = 0, len = this.events.length;
    while (i < len) {
      this.listeners[this.events[i]] = new Set ();
      i++;
    }
  }

  on (event, callback) {
    if (!this.listeners[event]) {
      this.listeners[event] = new Set ();
    }
    this.listeners[event].add (callback);
    return this;
  }

  off (event, callback) {
    if (!this.listeners[event]) return;
    this.listeners[event].delete (callback);
    return this;
  }

  emit (event) {
    if (arguments.length === 0) throw new Error ('No event given');

    const args = slice.call (arguments, 1);
    if (this.listeners[event]) {
      this.listeners[event].forEach (callback => {
        callback.apply (null, args);
      });
    }

    return this;
  }

  xform (transformers, opts) {
    return new XForm (this, transformers, opts);
  }

}

const slice = Array.prototype.slice;

export function emitter (events) {
  return new Emitter (events);
}

const transformHandleEvent =
  (event, xform, opts) => function () {
    const emitter = xform.emitter;
    let newValue = xform.transforms[event].apply (xform, arguments);

    if (newValue instanceof Promise) {
      return newValue.then (resolved => {
        if (!opts.multiReturn) {
          resolved = [resolved];
        }
        emitter.emit.bind (emitter, event).apply (emitter, resolved);
      });
    }

    if (!opts.multiReturn) {
      newValue = [newValue];
    }
    emitter.emit.bind (emitter, event).apply (emitter, newValue);
  };

export class XForm {

  constructor (source, transformers, opts) {
    if (!opts) { opts = {}; }
    if (!('multiReturn' in opts)) {
      opts.multiReturn = false;
    }

    this.emitter = new Emitter (Object.keys (transformers));
    this.transforms = transformers;

    const listenEvents = Object.keys (transformers);
    let i = 0, len = listenEvents.length;

    if (
      source instanceof XForm
      || source instanceof Emitter
    ) {
      while (i < len) {
        const evt = listenEvents[i];
        source.on (evt, transformHandleEvent (evt, this, opts));
        i++;
      }
    }

    else if (source instanceof EventTarget) {
      while (i < len) {
        const evt = listenEvents[i];
        source.addEventListener (evt, transformHandleEvent (evt, this, opts));
        i++;
      }
    }
  }

  on (event, listener) { 
    this.emitter.on (event, listener);
    return this;
  }

  off (event, listener) { 
    this.emitter.off (event, listener);
    return this;
  }

  xform (transformers, opts) {
    return new XForm (this, transformers, opts);
  }

}

export function xform (source, opts, transformers) {
  return new XForm (source, opts, transformers);
}
