/* eslint-disable max-len */
import { expect } from 'chai';

import { Emitter, xform, XForm } from './index.mjs';

describe ('@sprs/emitter', function () {
  describe ('class Emitter', function () {
    describe ('Instantiating an Emitter', function () {
      it ('should return an emitter with events property and listeners property populated', function () {
        const events = [
          'event_1',
          'event_2',
          'event_3',
          'event_4',
          'event_5',
        ];

        const emitter = new Emitter (events);

        expect (emitter).to.be.instanceof (Emitter); 
        expect (emitter.events).to.deep.equal (events);
        expect (emitter.listeners).to.deep.equal ({
          event_1 : new Set (),
          event_2 : new Set (),
          event_3 : new Set (),
          event_4 : new Set (),
          event_5 : new Set (),
        });
      });
    });

    describe ('Registering listeners, emitting events, and removing listeners', function () {
      it ('should call the listener only when it is registered', function () {
        let called = 0;
        const listener = () => { called += 1; };

        const emitter = new Emitter ();
        emitter.on ('A random event', listener);

        emitter.emit ('A random event');
        expect (called).to.equal (1);

        emitter.emit ('A random event');
        expect (called).to.equal (2);

        emitter.off ('A random event', listener);
        expect (called).to.equal (2);
      });
    });

    describe ('Registering a listener multiple times', function () {
      it ('should only result in a single subscription (idempotent subscription)', function () {
        let called = 0;
        const listener = () => { called += 1; };

        const emitter = new Emitter ();
        emitter.on ('@my!event', listener);
        emitter.on ('@my!event', listener);
        emitter.on ('@my!event', listener);

        emitter.emit ('@my!event');
        expect (called).to.equal (1);

        /*
         * Should only require single unsubscription,
         * even though subscribed more than once
         */
        emitter.off ('@my!event', listener);
        emitter.emit ('@my!event');
        expect (called).to.equal (1);
      });
    });

    describe ('Emitting an event to more than one listener', function () {
      it ('should call all listeners', function () {
        const emitter = new Emitter ();
        const called = {
          listener1 : false,
          listener2 : false,
          listener3 : false,
        };

        const listener1 = () => { called.listener1 = true; };
        const listener2 = () => { called.listener2 = true; };
        const listener3 = () => { called.listener3 = true; };

        emitter.on ('event', listener1);
        emitter.on ('event', listener2);
        emitter.on ('event', listener3);

        emitter.emit ('event');

        expect (called).to.deep.equal ({
          listener1 : true,
          listener2 : true,
          listener3 : true,
        });
      });
    });

    describe ('Emitting an event along with a payload', function () {
      it ('should pass that payload to listeners', function () {
        const emitter = new Emitter ();
        let emitted = null;

        emitter.on ('event', (...args) => { emitted = args; });
        emitter.emit ('event', 1, 2, 3, { this_is: 'my payload' });

        expect (emitted).to.deep.equal ([1, 2, 3, { this_is: 'my payload' }]);
      });
    });
  });

  describe ('class XForm', function () {
    describe ('Instantiating an XForm', function () {
      it ('should contain an internal emitter with a pre-declared event for each event transformer', function () {
        const emitter = new Emitter (['event1', 'event2']);

        const transforms = {
          event1 : num => num * 2,
          event2 : str => `Prefix - ${str} - Suffix`,
        };

        const inst = new XForm (emitter, transforms);

        expect (inst).to.be.instanceof (XForm);
        expect (inst.emitter).to.be.instanceof (Emitter);
        expect (inst.transforms).to.equal (transforms);
      });
    });

    describe ('Registering listeners, emitting events, and removing listeners', function () {
      it ('should call the listener only when it is registered, transforming the payload with the given transformer', function () {
        let result = null;

        const emitter = new Emitter (['event']);
        const inst = new XForm (emitter, { event: val => `Prefix - ${val} - Suffix` });
        inst.on ('event', transformedValue => { result = transformedValue; });

        emitter.emit ('event', 'my_value');

        expect (result).to.equal ('Prefix - my_value - Suffix');
      });
    });
  });

  describe ('function xform', function () {
    describe ('Calling the function on an Emitter', function () {
      it ('should return an XForm', function () {
        const emitter = new Emitter (['event']);
        const result = xform (emitter, { event: val => val * 2 });
        expect (result).to.be.instanceof (XForm);
      });
    });
  });
});
