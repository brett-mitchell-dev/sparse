// TODO: Add mechanism for declaring payload types per event

export declare class Emitter <const Events extends readonly string[]> {
  listeners: { [key: Events[number]]: Set };
  events: Events;

  constructor (events?: Events);

  on(event: Events[number], callback: Function): void;
  off(event: Events[number], callback: Function): void;
  emit(event: Events[number], ...args: any[]): void;

  xform
    <
      MultiReturn extends boolean = false,
      Transforms extends (
        MultiReturn extends true
          ? TransformersMultiReturn <Emitter <Events>>
          : TransformersSingleReturn <Emitter <Events>>
      )
    >
    (transforms: Transforms, opts?: { multiReturn?: MultiReturn })
    : XForm <Emitter <Events>, MultiReturn, Transforms>
}

export function emitter <const Events extends readonly string[]> (opts?: { events?: Events }): Emitter <Events>

type TransformersSingleReturn <Source extends XForm <any, any, any> | Emitter <any> | EventTarget> = (
  Source extends EventTarget
    ? { [key: string]: (event: Event) => any }
    : (
  Source extends Emitter <infer Events>
    ? & { [K in Events[number]]?: (...args: any[]) => any }
      & { [key: string]: (...args: any[]) => any }
    : (
  Source extends XForm <any, any, infer Transforms>
    ? & { [K in keyof Transforms]?: (...args: any[]) => any }
      & { [key: string]: (...args: any[]) => any }
    : never
  )
  )
)

type TransformersMultiReturn <Source extends XForm <any, any, any> | Emitter <any> | EventTarget> = (
  Source extends EventTarget
    ? { [key: string]: (event: Event) => any[] }
    : (
  Source extends Emitter <infer Events>
    ? & { [K in Events[number]]?: (...args: any[]) => any[] }
      & { [key: string]: (...args: any[]) => any[] }
    : (
  Source extends XForm <any, any, infer Transforms>
    ? & { [K in keyof Transforms]?: (...args: any[]) => any }
      & { [key: string]: (...args: any[]) => any }
    : never
  )
  )
)

export declare class XForm <
  Source extends XForm <any, any, any> | Emitter <any> | EventTarget,
  MultiReturn extends boolean,
  Transforms extends (
    MultiReturn extends true
      ? TransformersMultiReturn <Source>
      : TransformersSingleReturn <Source>
  ),
> {
  /** Internal, use `.on` and `.off` instead */
  emitter: Emitter <keyof Transforms>;
  transforms: Transforms;

  constructor(source: Source, transformers: Transforms);
  constructor(source: Source, transformers: Transforms, opts: { multiReturn?: MultiReturn });

  on <Event extends keyof Transforms> (event: Event, listener: (...args: Parameters <Transforms[Event]>) => ReturnType <Transforms[Event]>): void;
  on (event: string, listener: (value: any) => any): void;

  off <Event extends keyof Transforms> (event: Event, listener: (...args: any[]) => any): void;
  off (event: string, listener: (...args: any[]) => any): void;

  xform
    <
      NextMultiReturn extends boolean = false,
      NextTransforms extends (
        MultiReturn extends true
          ? TransformersMultiReturn <XForm <Source, MultiReturn, Transforms>>
          : TransformersSingleReturn <XForm <Source, MultiReturn, Transforms>>
      )
    >
    (transforms: NextTransforms, opts?: { multiReturn?: NextMultiReturn })
    : XForm <Emitter <Events>, NextMultiReturn, NextTransforms>
}

export function xform <
  Source extends XForm <any, any, any> | Emitter <any> | EventTarget,
  Transforms extends TransformersSingleReturn <Source>,
> (source: Source, transforms: Transforms)
  : XForm <Source, false, Transforms>;

export function xform <
  Source extends XForm <any, any, any> | Emitter <any> | EventTarget,
  MultiReturn extends boolean,
  Transforms extends (
    MultiReturn extends true
      ? TransformersMultiReturn <Source>
      : TransformersSingleReturn <Source>
  ),
> (source: Source, transforms: Transforms, opts: { multiReturn?: MultiReturn })
  : XForm <Source, MultiReturn, Transforms>;
