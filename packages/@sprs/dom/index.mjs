
import { Derived, Signal } from '@sprs/state';

export const html = makeElementFactory (createHtmlElement);
function createHtmlElement (tag) {
  return document.createElement (tag);
}

export const svg = makeElementFactory (createSvgElement);
function createSvgElement (tag) {
  return document.createElementNS ('http://www.w3.org/2000/svg', tag);
}

export const mathml = makeElementFactory (createMathMLElement);
function createMathMLElement (tag) {
  return document.createElementNS ('http://www.w3.org/1998/Math/MathML', tag);
}

export function render (componentArray) {
  if (!Array.isArray (componentArray)) {
    componentArray = [componentArray];
  }

  const fragment = document.createDocumentFragment ();

  let i = 0;
  while (i < componentArray.length) {
    const next = componentArray[i];
    i++;

    if (Array.isArray (next)) {
      fragment.append (render (next));
      continue;
    }
    else if (next instanceof Node) {
      fragment.append (next);
      continue;
    }
    else if (next instanceof NodeProducer) {
      const node = next.getNode ();
      if (node instanceof Node || typeof node === 'string') {
        fragment.append (node);
      }
    }
    else if (next instanceof Signal || next instanceof Derived) {
      const node = bindStateToDom (next);
      fragment.append (node);
    }
    else if (next === null || typeof next === 'undefined') {
      // pass
    }
    else {
      const rendered = document.createTextNode (next.toString ());
      fragment.append (rendered);
    }
  }

  return fragment;
}

export class NodeProducer {

  getNode () { return document.createDocumentFragment (); }

}

export class Peg extends NodeProducer {

  constructor (initialChild) {
    super ();
    this.attachedNodes = [];

    this.node = document.createElement ('div');
    this.node.setAttribute ('x-sprs-peg', '');
    this.node.setAttribute ('hidden', '');

    if (initialChild) {
      this.affix (initialChild);
    }
  }

  remove () {
    this.removeChildren ();
    this.node.remove ();
  }

  removeChildren () {
    const nodesToRemove = this.attachedNodes;
    const parent = this.node.parentNode;
    if (!parent) return false;

    // Throw-away fragment to remove nodes with a single reflow
    let trashBin = document.createDocumentFragment ();
    let removalIndex = 0, len = nodesToRemove.length;
    while (removalIndex < len) {
      const removeTarget = nodesToRemove[removalIndex];
      if (removeTarget.parentNode === parent) {
        trashBin.append (removeTarget);
      }
      removalIndex++;
    }

    return true;
  }

  getNode () {
    const fragment = document.createDocumentFragment ();
    fragment.append (this.node);
    fragment.append.apply (fragment, this.attachedNodes);
    return fragment;
  }

  clear () {
    this.affix (document.createDocumentFragment ());
  }

  affix (newChild) {
    if (!(newChild instanceof Node)) {
      newChild = render (newChild);
    }

    const removedChildren = this.removeChildren ();

    if (newChild instanceof DocumentFragment) {
      this.attachedNodes = slice.call (newChild.childNodes, 0);
    }
    else {
      this.attachedNodes = [newChild];
    }

    if (removedChildren) {
      this.node.parentNode.insertBefore (newChild, this.node.nextSibling);
    }
  }

}

const slice = Array.prototype.slice;

/**
 * Reactive is used to replace a known portion of
 * the DOM when something happens.
 */
export class Reactive extends NodeProducer {

  constructor (fn) {
    if (fn && typeof fn !== 'function') {
      throw new Error ('Reactive : Optional parameter \'fn\' must be a function');
    }

    super ();
    this.fn = fn;
    this.onValue = this.onValue.bind (this);
    this.peg = new Peg ();
    this.previousValue = null;
  }

  getNode () { return this.peg.getNode (); }
  remove () { this.peg.remove (); }

  onValue (newValue) {
    if (this.fn) {
      newValue = this.fn (
        newValue,
        { previous: this.previousValue, nodes: this.peg.attachedNodes },
      );
    }

    if (newValue instanceof Promise) {
      newValue.then (resolved => {
        this.previousValue = resolved;
        const fragment = render (resolved);
        this.peg.affix (fragment);
      });
    }

    else {
      this.previousValue = newValue;
      const fragment = render (newValue);
      this.peg.affix (fragment);
    }
  }

}

function setAttr (elem, attrName, attrValue) {
  if (attrName === 'style') {
    if (typeof attrValue === 'string') {
      elem.style = attrValue;
    }
    else if (typeof attrValue === 'undefined' || attrValue === null) {
      elem.style = '';
    }
    else if (attrValue instanceof Signal || attrValue instanceof Derived) {
      bindStateAttr (elem, 'style', attrValue);
    }
    else if (typeof attrValue === 'object') {
      const attrNames = Object.keys (attrValue);
      let attrIndex = 0, cbLen = attrNames.length;
      while (attrIndex < cbLen) {
        const styleValue = attrValue[attrNames[attrIndex]];
        if (styleValue instanceof Signal || styleValue instanceof Derived) {
          bindStateAttr (elem.style, attrNames[attrIndex], styleValue);
        }
        else {
          elem.style[attrNames[attrIndex]] = attrValue[attrNames[attrIndex]];
        }
        attrIndex++;
      }
    }
    return;
  }

  if (typeof attrValue === 'undefined') {
    elem.removeAttribute (attrName);
    return;
  }

  if (typeof attrValue === 'boolean') {
    if (attrValue) {
      elem.setAttribute (attrName, '');
    }
    else {
      elem.removeAttribute (attrName);
    }
    return;
  }

  if (attrValue instanceof Signal || attrValue instanceof Derived) {
    bindStateAttr (elem, attrName, attrValue);
    return;
  }

  elem.setAttribute (attrName, attrValue.toString ());
}

export function setAttrs (element, attrMap) {
  if (!attrMap) return element;

  const attrNames = Object.keys (attrMap);
  let attrIndex = 0, cbLen = attrNames.length;
  while (attrIndex < cbLen) {
    if (attrNames[attrIndex] !== 'on') {
      setAttr (element, attrNames[attrIndex], attrMap[attrNames[attrIndex]]);
    }
    attrIndex++;
  }

  return element;
}

export function applyListeners (element, listenerMap) {
  if (!element) return element;
  if (!listenerMap) return element;

  if (listenerMap.on) {
    listenerMap = listenerMap.on;
  }
  listenerMap = listenerMap || IMMUTABLE_EMPTY_OBJ;

  const eventNames = Object.keys (listenerMap);
  let eventNameIndex = 0, cbLen = eventNames.length;
  while (eventNameIndex < cbLen) {
    const listener = listenerMap[eventNames[eventNameIndex]];
    if (listener instanceof Signal || listener instanceof Derived) {
      bindListenerToCallbackState (element, eventNames[eventNameIndex], listener);
    }
    else {
      element.addEventListener (
        eventNames[eventNameIndex],
        listener,
      );
    }
    eventNameIndex++;
  }

  return element;
}

function makeElementFactory (createElement) {
  return function (tag, arg1, arg2) {
    let opts = arg1, children = arg2;
    if (typeof arg2 === 'undefined') {
      if (Array.isArray (arg1)) {
        opts = IMMUTABLE_EMPTY_OBJ;
        children = arg1;
      } else {
        children = IMMUTABLE_EMPTY_ARR;
      }
    }

    if (typeof opts !== 'object' || !opts) {
      opts = IMMUTABLE_EMPTY_OBJ;
    }

    let element;
    if (typeof tag === 'string') {
      element = createElement (tag);
    }
    else if (tag instanceof Node) {
      element = tag.cloneNode (true);
    }
    else {
      throw new Error ('Invalid parameter, must be a string or a Node');
    }

    setAttrs (element, opts);
    applyListeners (element, opts.on);

    if (Array.isArray (children)) {
      const renderedChildren = render (children);
      element.append (renderedChildren);
    }

    return element;
  };
}

const IMMUTABLE_EMPTY_OBJ = Object.freeze ({});
const IMMUTABLE_EMPTY_ARR = Object.freeze ([]);

/**
 * Binds a signal to a property on an object.
 * Uses WeakRef and FinalizationRegistry to prevent memory leaks.
 */
function bindStateAttr (targetObj, attrName, state) {
  const reference = new WeakRef (targetObj);

  let binding;
  binding = newValue => {
    const target = reference.deref ();
    if (!target) return;
    target[attrName] = newValue;
  };

  binding.finalizer = new FinalizationRegistry (() => {
    state.off ('value', binding);
  });
  binding.finalizer.register (targetObj);

  state.on ('value', binding);
  targetObj[attrName] = state.get ();
}

export const BINDING_SYMBOL = Symbol ('sprs.binding');
/**
 * Binds a signal to a reactive element.
 * Uses WeakRef and FinalizationRegistry to prevent memory leaks.
 */
function bindStateToDom (state) {
  const reactive = new Reactive ();
  const reactiveRef = new WeakRef (reactive);

  /*
   * Keep a reference to the reactive instance on the node itself.
   * This allows the node's lifetime to determine the lifetime of
   * the state binding.
   */
  reactive.peg.node[BINDING_SYMBOL] = reactive;

  const binding = (...args) => {
    const derefedReactive = reactiveRef.deref ();
    if (!derefedReactive) return;
    derefedReactive.onValue (...args);
  };

  binding.finalizer = new FinalizationRegistry (() => {
    state.off ('value', binding);
  });
  binding.finalizer.register (reactive);

  state.on ('value', binding);
  
  reactive.onValue (state.get ());
  return reactive.getNode ();
}

/**
 * Binds a signal as an event listener on an element.
 * Uses WeakRef and FinalizationRegistry to prevent memory leaks.
 */
function bindListenerToCallbackState (elem, eventName, state) {
  const elemRef = new WeakRef (elem);

  const binding = (newCb, { previous: previousCb }) => {
    const derefedElem = elemRef.deref ();
    if (!derefedElem) return;

    if (typeof previousCb === 'function') {
      derefedElem.removeEventListener (eventName, previousCb);
    }
    if (typeof newCb === 'function') {
      derefedElem.addEventListener (eventName, newCb);
    }
  };

  binding.finalizer = new FinalizationRegistry (() => {
    state.off ('value', binding);
  });
  binding.finalizer.register (elem);

  state.on ('value', binding);
  elem.addEventListener (eventName, state.get ());
}
