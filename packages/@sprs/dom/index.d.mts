
// Piggy-back off of the React team's amazing work on HTML attribute documentation...
import { JSX, HTMLAttributes, DOMAttributes } from '@types/react'
import { Signal, Derived } from '@sprs/state'

export class NodeProducer {
  getNode(): Node;
}

/**
  * Peg maintains a fixed location in the DOM using a
  * hidden node.
  *
  * 'affix' will replace the current contents with the
  * nodes passed to the function.
  *
  * 'clear' will remove the current contents from the
  * document.
  *
  * 'remove' will remove the entire structure, including
  * the hidden node used to maintain reference position.
  */
export class Peg extends NodeProducer {
  constructor(initialChild?: Node);

  remove(): void;
  clear(): void;
  affix(newChild: Node): void;

  attachedNodes: Node[];
  node: Node;
}

/**
 * Replaces a part of the document when its value changes
 */
export class Reactive extends NodeProducer {
  fn?: Function;
  peg: Peg;
  previousValue: any;

  remove(): void;
  onValue(newValue: any): void;
}

type ReactSpecificKeys = (
  | 'key'
  | 'ref'
  | 'defaultChecked'
  | 'defaultValue'
  | 'suppressContentEditableWarning'
  | 'suppressHydrationWarning'
  | keyof DOMAttributes <HTMLElement>
)

type AllowStates <T> = { [K in keyof T]: T[K] | Signal <any> | Derived <any, any> }

type Opts <
  K,
  Attributes = (
    // K extends Node
    //   // TODO: Specify this better.
    //   // Probably will require mapping actual node type to key of JSX.IntrinsicElements
    //   ? Omit <HTMLAttributes <HTMLElementTagNameMap[keyof HTMLElementTagNameMap]>, ReactSpecificKeys> 
    //   : (
    K extends keyof JSX.IntrinsicElements
      ? Omit <JSX.IntrinsicElements[K], ReactSpecificKeys>
      : HTMLAttributes <HTMLElementTagNameMap[K]>
    )
  // )
> = (
  AllowStates <Omit<Attributes, 'style'>> & {
    style?: string | AllowStates <Attributes['style']>;
    // TODO: Use React's type definitions for event handlers rather than
    //       lib.d.ts. React provides better completions for the specific
    //       target type, e.g. clicking a button has event.target as a button,
    //       rather than a generic EventTarget.
    on?: AllowStates <{
      [K in keyof GlobalEventHandlersEventMap]?:
        (event: GlobalEventHandlersEventMap[K]) => any
    }>;
  }
)

export type Renderable = (
  | Node
  | NodeProducer
  | Signal <any>
  | Derived <any, any>
  | string
  | number
  | boolean
  | Renderable[]
);

export function html <K extends keyof HTMLElementTagNameMap> (tag: K): HTMLElementTagNameMap[K];
export function html <K extends keyof HTMLElementTagNameMap> (tag: K, opts: Opts <K>): HTMLElementTagNameMap[K];
export function html <K extends keyof HTMLElementTagNameMap> (tag: K, opts: Opts <K>, children: Renderable[]): HTMLElementTagNameMap[K];
export function html <K extends keyof HTMLElementTagNameMap> (tag: K, children: Renderable[]): HTMLElementTagNameMap[K];
export function html <E extends HTMLElement> (element: E): E;
export function html <E extends HTMLElement> (element: E, opts: Opts <K>): E;
export function html <E extends HTMLElement> (element: E, opts: Opts <K>, children: Renderable[]): E;
export function html <E extends HTMLElement> (element: E, children: Renderable[]): E;

export function svg <K extends keyof SVGElementTagNameMap> (tag: K): SVGElementTagNameMap[K];
export function svg <K extends keyof SVGElementTagNameMap> (tag: K, opts: Opts <K>): SVGElementTagNameMap[K];
export function svg <K extends keyof SVGElementTagNameMap> (tag: K, opts: Opts <K>, children: Renderable[]): SVGElementTagNameMap[K];
export function svg <K extends keyof SVGElementTagNameMap> (tag: K, children: Renderable[]): SVGElementTagNameMap[K];
export function svg <E extends SVGElement> (element: E): E;
export function svg <E extends SVGElement> (element: E, opts: Opts <K>): E;
export function svg <E extends SVGElement> (element: E, opts: Opts <K>, children: Renderable[]): E;
export function svg <E extends SVGElement> (element: E, children: Renderable[]): E;

export function mathml <K extends keyof MathMLElementTagNameMap> (tag: K): MathMLElementTagNameMap[K];
export function mathml <K extends keyof MathMLElementTagNameMap> (tag: K, opts: Opts <K>): MathMLElementTagNameMap[K];
export function mathml <K extends keyof MathMLElementTagNameMap> (tag: K, opts: Opts <K>, children: Renderable[]): MathMLElementTagNameMap[K];
export function mathml <K extends keyof MathMLElementTagNameMap> (tag: K, children: Renderable[]): MathMLElementTagNameMap[K];
export function mathml <E extends MathMLElement> (element: E): E;
export function mathml <E extends MathMLElement> (element: E, opts: Opts <K>): E;
export function mathml <E extends MathMLElement> (element: E, opts: Opts <K>, children: Renderable[]): E;
export function mathml <E extends MathMLElement> (element: E, children: Renderable[]): E;

export function render (items: Renderable): DocumentFragment;

export function setAttrs <N extends Node> (element: N, opts: Opts <N>): N
export function applyListeners <N extends Node> (element: N, opts: Opts <N> | Opts <N> ['on']): N
