import { NodeProducer } from '@sprs/dom';

export type DROP_ZONE_SPRS_EVENT = (
  | 'dragenter'
  | 'dragover'
  | 'dragleave'
  | 'drop'
);

export type DROP_ZONE_NATIVE_EVENT = (
  | 'nativedragenter'
  | 'nativedragover'
  | 'nativedragleave'
  | 'nativedrop'
);

export type DROP_ZONE_EVENT = (
  | DROP_ZONE_SPRS_EVENT
  | DROP_ZONE_NATIVE_EVENT
)

type SprsEventHandler <E extends HTMLElement> = (
  (payload: {
    self: DropZone <E>;
    droppedDragAnchor: DragAnchor <HTMLElement>;
    event: MouseEvent;
    dragOffset: Point;
    dragOrigin: Point;
  }) => any
);

type DropEventHandlers <E extends HTMLElement> = { [K in DROP_ZONE_EVENT]?: SprsEventHandler <E> };

type DropZoneOpts = {
  sprs?: boolean;
  native?: boolean;
}

export class DropZone <E extends HTMLElement> extends NodeProducer {
  element: E;
  opts: DropZoneOpts;
  emitter: Emitter <readonly [
    'dragenter',
    'dragover',
    'dragleave',
    'drop',
    'nativedragenter',
    'nativedragover',
    'nativedragleave',
    'nativedrop',
  ]>

  constructor(element: E, opts?: DropZoneOpts);

  on(event: DROP_ZONE_SPRS_EVENT, SparseEventHandler): void;
  on(event: DROP_ZONE_NATIVE_EVENT, NativeEventHandler): void;

  off(event: DROP_ZONE_SPRS_EVENT, SparseEventHandler): void;
  off(event: DROP_ZONE_NATIVE_EVENT, NativeEventHandler): void;

  activate(): void;
  deactivate(): void;
}

export function dropzone
  <E extends HTMLElement>
  (element: E, opts?: DropZoneOpts & { on?: DropEventHandlers <E> })
    : DropZone <E>;
