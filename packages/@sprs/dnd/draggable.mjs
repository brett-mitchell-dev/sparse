import { NodeProducer, Peg } from '@sprs/dom';
import { Emitter } from '@sprs/emitter';

import { getDragEvents } from './getDragEvents.mjs';

export class DragAnchor extends NodeProducer {

  constructor (dragTarget, opts = {}) {
    super ();

    this.dragTarget = dragTarget;
    this.dragRegion = this.dragTarget;
    if (opts.dragRegion instanceof Node) {
      this.dragRegion = opts.dragRegion;
    }

    if (opts.preventTextSelection !== false) {
      this.dragRegion.style.userSelect = 'none';
    }

    this.previousHoverTarget = null;
    this.elementStyleCache = '';
    this.peg = new Peg (dragTarget);

    this.dragThreshold = opts.dragThreshold || 0;
    this.dragEvents = getDragEvents (this.dragRegion, opts);

    this.emitter = new Emitter ([
      'dragpending',
      'beforedragstart',
      'afterdragstart',
      'beforedragmove',
      'afterdragmove',
      'beforedragend',
      'afterdragend',
      'beforedragcancel',
      'afterdragcancel',
    ]);

    this.dragPendingCb = this.onDragPending.bind (this);
    this.dragStartCb = this.onDragStart.bind (this);
    this.dragMoveCb = this.onDragMove.bind (this);
    this.dragEndCb = this.onDragEnd.bind (this);
    this.dragCancelCb = this.onDragCancel.bind (this);
  }

  on (event, callback) {
    this.emitter.on (event, callback);
  }

  getNode () {
    return this.peg.getNode ();
  }

  activate () {
    this.elementStyleCache = this.dragTarget.style.cssText;

    this.dragEvents.on ('dragpending', this.dragPendingCb);
    this.dragEvents.on ('dragstart', this.dragStartCb);
    this.dragEvents.on ('dragmove', this.dragMoveCb);
    this.dragEvents.on ('dragend', this.dragEndCb);
    this.dragEvents.on ('dragcancel', this.dragCancelCb);
  }

  deactivate () {
    this.onDragEnd ();

    this.dragEvents.off ('dragpending', this.dragPendingCb);
    this.dragEvents.off ('dragstart', this.dragStartCb);
    this.dragEvents.off ('dragmove', this.dragMoveCb);
    this.dragEvents.off ('dragend', this.dragEndCb);
    this.dragEvents.off ('dragcancel', this.dragCancelCb);
  }

  returnElement () {
    this.dragTarget.style.cssText = this.elementStyleCache;
    this.peg.affix (this.dragTarget);
    this.previousHoverTarget = null;
  }

  emitEventOnTarget (target, eventType, eventPayload) {
    if (target instanceof Node) {
      const event = new CustomEvent (eventType, {
        detail: {
          droppedDragAnchor: this,
          ...eventPayload,
        },
      });
      target.dispatchEvent (event);
    }
  }

  onDragCancel (payload) {
    this.emitter.emit ('beforedragcancel', { self: this, ...payload });
    this.emitEventOnTarget (this.previousHoverTarget, 'x-sprs-dragleave', payload);
    this.returnElement ();
    this.emitter.emit ('afterdragcancel', { self: this, ...payload });
  }

  onDragEnd (payload) {
    this.emitter.emit ('beforedragend', { self: this, ...payload });
    this.emitEventOnTarget (this.previousHoverTarget, 'x-sprs-dragleave', payload);
    this.emitEventOnTarget (payload.event.target, 'x-sprs-drop', payload);
    this.returnElement ();
    this.emitter.emit ('afterdragend', { self: this, ...payload });
  }

  onDragPending ({ event }) {
    if (event.target instanceof HTMLElement && event.target.hasPointerCapture (event.pointerId)) {
      event.target.releasePointerCapture (event.pointerId);
    }
    event.stopImmediatePropagation ();
  }

  onDragMove (payload) {
    this.emitter.emit ('beforedragmove', { self: this, ...payload });

    const { dragOffset, event } = payload;
    this.position.x = event.clientX + dragOffset.x;
    this.position.y = event.clientY + dragOffset.y;
    this.dragTarget.style.left = `${this.position.x}px`;
    this.dragTarget.style.top = `${this.position.y}px`;

    if (this.previousHoverTarget === event.target) {
      this.emitEventOnTarget (event.target, 'x-sprs-dragover', payload);
    } else {
      this.emitEventOnTarget (this.previousHoverTarget, 'x-sprs-dragleave', payload);
      this.emitEventOnTarget (event.target, 'x-sprs-dragenter', payload);
      this.previousHoverTarget = event.target;
    }

    this.emitter.emit ('afterdragmove', { self: this, ...payload });
  }

  onDragStart (payload) {
    this.emitter.emit ('beforedragstart', { self: this, ...payload });

    const boundingRect = this.dragTarget.getBoundingClientRect ();
    const computedStyles = window.getComputedStyle (this.dragTarget);
    const x = boundingRect.left - parseFloat (computedStyles.marginLeft);
    const y = boundingRect.top - parseFloat (computedStyles.marginTop);
    this.position = { x, y };

    this.dragTarget.style.position = 'fixed';
    this.dragTarget.style.left = `${x}px`;
    this.dragTarget.style.top = `${y}px`;
    this.dragTarget.style.width = `${boundingRect.width}px`;
    this.dragTarget.style.height = `${boundingRect.height}px`;

    /*
     * Allows pointerup event to pass through dragged element
     * to element underneath for drop interaction.
     */
    this.dragTarget.style.pointerEvents = 'none';

    const bodyElement = document.getElementsByTagName ('body')[0];
    if (!bodyElement) {
      console.log ('Unsupported HTML structure: Could not find "body" tag');
      return;
    }

    this.peg.clear ();
    bodyElement.appendChild (this.dragTarget);

    this.emitter.emit ('afterdragstart', { self: this, ...payload });
  }

}

export const draggable = (dragRegion, opts = {}) => {
  const dragAnchor = new DragAnchor (dragRegion, opts);
  dragAnchor.activate ();

  const handlers = opts.on || {};

  let i = 0, len = dragAnchor.emitter.events.length;
  while (i < len) {
    const event = dragAnchor.emitter.events[i];
    if (typeof handlers[event] === 'function') {
      dragAnchor.on (event, handlers[event]);
    }
    i++;
  }

  return dragAnchor;
};
