import { Emitter } from '@sprs/emitter';
import { Peg, NodeProducer } from '@sprs/dom';

type Point = { x: number, y: number };

type DragEventHandler <E extends HTMLElement> = (
  (payload: {
    self: DragAnchor <E>;
    event: MouseEvent;
    dragOffset: Point;
    dragOrigin: Point;
  }) => any
);

export type DRAG_ANCHOR_EVENT = (
  | 'dragpending'
  | 'beforedragstart'
  | 'afterdragstart'
  | 'beforedragmove'
  | 'afterdragmove'
  | 'beforedragend'
  | 'afterdragend'
  | 'beforedragcancel'
  | 'afterdragcancel'
);

type DropEventHandlers <E extends HTMLElement> = { [K in DRAG_ANCHOR_EVENT]?: DragEventHandler <E> };

type DragAnchorOpts = {
  dragRegion?: Node;
  dragThreshold?: number;
  maintainNegativeSpace?: boolean;
  preventTextSelection?: boolean;
};

type DraggableOpts <E extends HTMLElement> = (
  & DragAnchorOpts
  & { on?: Partial <DropEventHandlers <E>> }
)

export class DragAnchor <E extends HTMLElement> extends NodeProducer {
  dragRegion: E;
  dragTarget: HTMLElement;

  elementStyleCache: string;
  dragThreshold: number;
  peg: Peg;
  dragEvents: Emitter <readonly [
    'dragpending',
    'dragstart',
    'dragmove',
    'dragend',
    'dragcancel',
  ]>;

  emitter: Emitter <readonly [
    'dragpending',
    'beforedragstart',
    'afterdragstart',
    'beforedragmove',
    'afterdragmove',
    'beforedragend',
    'afterdragend',
    'beforedragcancel',
    'afterdragcancel',
  ]>

  constructor (dragTarget: E, opts?: DragAnchorOpts);

  on(event: DRAG_ANCHOR_EVENT, callback: (...args: any[]) => any): void;

  activate(): void;
  deactivate(): void;

  onDragPending(): void;
  onDragStart(): void;
  onDragMove(): void;
  onDragEnd(): void;
  onDragCancel(): void;
}

export function draggable
  <E extends HTMLElement>
  (dragTarget: E, opts?: DraggableOpts <E>)
    : DragAnchor <E>;
