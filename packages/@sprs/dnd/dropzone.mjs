import { NodeProducer } from '@sprs/dom';
import { Emitter } from '@sprs/emitter';

export class DropZone extends NodeProducer {

  constructor (element, opts = {}) {
    super ();

    this.element = element;
    this.opts = opts;

    this.emitter = new Emitter ([
      'dragenter',
      'dragover',
      'dragleave',
      'drop',
      'nativedragenter',
      'nativedragover',
      'nativedragleave',
      'nativedrop',
    ]);

    this.onDropCb = this.onDrop.bind (this);
    this.onDragEnterCb = this.onDragEnter.bind (this);
    this.onDragOverCb = this.onDragOver.bind (this);
    this.onDragLeaveCb = this.onDragLeave.bind (this);

    this.onNativeDropCb = this.onNativeDrop.bind (this);
    this.onNativeDragEnterCb = this.onNativeDragEnter.bind (this);
    this.onNativeDragOverCb = this.onNativeDragOver.bind (this);
    this.onNativeDragLeaveCb = this.onNativeDragLeave.bind (this);
  }

  getNode () {
    return this.element;
  }

  on (event, callback) {
    this.emitter.on (event, callback);
  }

  off (event, callback) {
    this.emitter.off (event, callback);
  }

  activate () {
    if (this.opts.sprs !== false) {
      this.element.addEventListener ('x-sprs-drop', this.onDropCb);
      this.element.addEventListener ('x-sprs-dragenter', this.onDragEnterCb);
      this.element.addEventListener ('x-sprs-dragover', this.onDragOverCb);
      this.element.addEventListener ('x-sprs-dragleave', this.onDragLeaveCb);
    }

    if (this.opts.native === true) {
      this.element.addEventListener ('drop', this.onNativeDropCb);
      this.element.addEventListener ('dragenter', this.onNativeDragEnterCb);
      this.element.addEventListener ('dragover', this.onNativeDragOverCb);
      this.element.addEventListener ('dragleave', this.onNativeDragLeaveCb);
    }
  }

  deactivate () {
    this.element.removeEventListener ('x-sprs-drop', this.onDropCb);
    this.element.removeEventListener ('x-sprs-dragenter', this.onDragEnterCb);
    this.element.removeEventListener ('x-sprs-dragover', this.onDragOverCb);
    this.element.removeEventListener ('x-sprs-dragleave', this.onDragLeaveCb);

    this.element.removeEventListener ('drop', this.onNativeDropCb);
    this.element.removeEventListener ('dragenter', this.onNativeDragEnterCb);
    this.element.removeEventListener ('dragover', this.onNativeDragOverCb);
    this.element.removeEventListener ('dragleave', this.onNativeDragLeaveCb);
  }

  onDrop (event) {
    this.emitter.emit ('drop', { ...event.detail, self: this });
  }

  onDragEnter (event) {
    this.emitter.emit ('dragenter', { ...event.detail, self: this });
  }

  onDragOver (event) {
    this.emitter.emit ('dragover', { ...event.detail, self: this });
  }

  onDragLeave (event) {
    this.emitter.emit ('dragleave', { ...event.detail, self: this });
  }

  onNativeDrop (event) {
    this.emitter.emit ('nativedrop', event);
  }

  onNativeDragEnter (event) {
    /*
     * This event must have its default prevented in order for the browser
     * drag and drop API to recognize the element as a valid drop target
     */
    event.preventDefault ();
    this.emitter.emit ('nativedragenter', event);
  }

  onNativeDragOver (event) {
    /*
     * This event must have its default prevented in order for the browser
     * drag and drop API to recognize the element as a valid drop target
     */
    event.preventDefault ();
    this.emitter.emit ('nativedragover', event);
  }

  onNativeDragLeave (event) {
    this.emitter.emit ('nativedragleave', event);
  }

}

/**
 * @param {HTMLElement} element
 */
export const dropzone = (element, opts = {}) => {
  const instance = new DropZone (element, opts);
  instance.activate ();

  const handlers = opts.on || {};

  let i = 0, len = instance.emitter.events.length;
  while (i < len) {
    const event = instance.emitter.events[i];
    if (typeof handlers[event] === 'function') {
      instance.on (event, handlers[event]);
    }
    i++;
  }

  return instance;
};
