import { Emitter } from '@sprs/emitter';

/**
 * @typedef {{ dragThreshold?: number }} Opts
 */

export const STATE = Object.freeze ({
  INACTIVE : 'inactive',
  PENDING  : 'pending',
  DRAGGING : 'dragging',
});

/**
 * Constructs events for drag interactions on an HTML element.
 * Accepts `dragThreshold` option as a number measured in pixels,
 * which prevents drag from being initiated until the pointer
 * has been moved by at least that distance from the pointer
 * interaction origin.
 *
 * @param {HTMLElement} elem Element to construct drag events for
 * @param {Opts} [opts] Options: `dragThreshold`
 *
 * TODO: Add explicit type declarations for each event payload
 */
export const getDragEvents = (elem, opts = {}) => {
  const emitter = new Emitter ([
    'dragpending',
    'dragstart',
    'dragmove',
    'dragend',
    'dragcancel',
  ]);

  let dragThreshold = opts.dragThreshold || 0;
  dragThreshold = dragThreshold * dragThreshold;

  /** @type {STATE[keyof STATE]} */
  let state = STATE.INACTIVE;
  let dragOrigin = { x: 0, y: 0 };
  let dragOffset = { x: 0, y: 0 };
  let lastEvent = null;

  elem.addEventListener ('pointerdown', event => {
    if (state === STATE.INACTIVE) {
      const boundingRect = elem.getBoundingClientRect ();
      const computedStyles = window.getComputedStyle (elem);

      dragOffset = {
        x : boundingRect.left - event.clientX - parseFloat (computedStyles.marginLeft),
        y : boundingRect.top - event.clientY - parseFloat (computedStyles.marginTop),
      };

      dragOrigin = {
        x : event.clientX,
        y : event.clientY,
      };

      lastEvent = event;
      state = STATE.PENDING;
      emitter.emit ('dragpending', { event, dragOffset, dragOrigin });
    }
  });

  window.addEventListener ('pointermove', event => {
    if (state === STATE.DRAGGING) {
      lastEvent = event;
      emitter.emit ('dragmove', { event, dragOffset, dragOrigin });
      return;
    }

    if (state === STATE.PENDING) {
      const deltaX = event.clientX - dragOrigin.x;
      const deltaY = event.clientY - dragOrigin.y;
      if (deltaX * deltaX + deltaY * deltaY > dragThreshold) {
        state = STATE.DRAGGING;
        emitter.emit ('dragstart', { event, dragOffset, dragOrigin });
      }
    }
  });

  window.addEventListener ('pointerup', event => {
    if (state === STATE.DRAGGING) {
      lastEvent = event;
      emitter.emit ('dragend', { event, dragOffset, dragOrigin });
    }

    dragOrigin = { x: 0, y: 0 };
    dragOffset = { x: 0, y: 0 };
    state = STATE.INACTIVE;
  });

  /** @param {any} [payload] */ // @ts-ignore
  emitter.cancel = (payload = {}) => {
    emitter.emit ('dragcancel', {
      ...payload,
      lastEvent,
      dragOffset,
      dragOrigin,
      canceledState: state,
    });

    dragOrigin = { x: 0, y: 0 };
    dragOffset = { x: 0, y: 0 };
    state = STATE.INACTIVE;
  };

  return emitter;
};
