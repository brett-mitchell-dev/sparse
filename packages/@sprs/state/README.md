
# `@sprs/state`

`@sprs/state` is a minimal library for dealing with reactive states.

Its feature set is built on top of event emitters. <!-- TODO with listSignal and path subscription: , and is capable of supporting fine-grained reactivity use cases. -->

## Utilities

### `signal(init)`

A `signal` wraps a value and is capable of notifying other parts of your application when its value has changed.

`signal`s provide the following methods:
- `get`: Get the current value
- `set`: Set the current value
- `mut`: Transform the current value with a function
- `on`: Subscribe to changes to the value
- `off`: Unsubscribe from changes to the value
- `derive`: Create a derived signal (often called a `computed` by other frameworks/libraries). More on this later on.

Example:

```js

import { signal } from `@sprs/state`;

const count = signal(0);
const listener = v => console.log('The value is: ', v);
count.on('value', listener);

count.set(100);
// -> 'The value is: 100'

count.mut(prev => prev + 10);
// -> 'The value is: 110'

count.off('value', listener);
count.set(1000);
// -> No message

```

### `derive(source, init, derivers)` / `derive(source, derivers)`

`derive` is used to create derived signals from other sources of information.

`derive`d signals offer the following instance methods:
- `on`: Subscribe to changes in value
- `off`: Unsubscribe from changes in value
- `derive`: Derive a new signal from this one

`derive` accepts `signal`s, `Emitter` and `XForm` instances from `@sprs/emitter`, and browser-native `EventTarget`s.

When the source is a `signal`, the intitial value is optional, as it can be automatically pulled from the source signal. Additionally, in this case the deriver parameter may be a single function to be applied to the `value` event.

When the source is any sort of event emitter, the initial value is required, and the derivers parameter must be an object with event names as keys, and derivation functions as values.

`signal` example:

```js

import { signal, derive } from '@sprs/state';

const count = signal(0);
const doubledCount = derive(count, value => value * 2);

doubledCount.get(); // -> 0
count.set(100);
doubledCount.get(); // -> 200

// Note that the same effect can be had with the `derive` instance method
const instanceDerivation = count.derive(value => value * 2);

instanceDerivation.get(); // -> 0
count.set(100);
instanceDerivation.get(); // -> 200

```

`EventEmitter` example:

```js

import { derive } from '@sprs/state';

const button = document.createElement('button');
const lastKnownMouseOverPos = derive(
  button,
  { x: 0, y: 0 },
  { mouseover: event => ({ x: event.clientX, y: event.clientY }) }
);

document.body.appendChild(button);

// Any time the mouse moves over the button, the
// derived state 'lastKnownMouseOverPos' will be updated
// with the pointer's viewport coordinates.

```

### `joini(sources)` (`joinImmediate(sources)`) / `joind(sources)` (`joinDeferred(sources)`)

The `join` family of functions is used to combine multiple signals into one. The value wrapped by the resulting derived signal will be an array, whose elements contain the values of each of the source signals in the same order they were specified.

The immediate variation, `joini`, will update the derived value immediately every time one of the upstream signals is updated.

The deferred variation, `joind`, will wait to update the derived value using a microtask.

```js

import { signal, joini, joind } from '@sprs/state';

const s1 = signal(1);
const s2 = signal(2);
const s3 = signal(3);

// -- joini -- //

const joinedImmediate = joini([s1, s2, s3]);
joinedImmediate.on('value', () => console.log('updated'));

joinedImmediate.get(); // -> [1, 2, 3]
s1.set(10);
// -> Logs 'updated'
joinedImmediate.get(); // -> [10, 2, 3]
s2.set(20);
// -> Logs 'updated'
joinedImmediate.get(); // -> [10, 20, 3]
s3.set(30);
// -> Logs 'updated'
joinedImmediate.get(); // -> [10, 20, 30]

s1.set(1);
s2.set(2);
s3.set(3);

// -- joind -- //

const joinedDeferred = joind([s1, s2, s3]);
joinedDeferred.on('value', () => console.log('updated'));

joinedDeferred.get(); // -> [1, 2, 3]
s1.set(10);
// No log
joinedDeferred.get(); // -> [1, 2, 3]
s2.set(20);
// No log
joinedDeferred.get(); // -> [1, 2, 3]
s3.set(30);
// No log
joinedDeferred.get(); // -> [1, 2, 3]

queueMicrotask(() => {
  // -> Logs 'updated' in previous microtask
  joinedDeferred.get(); // -> [10, 20, 30]
});

```

