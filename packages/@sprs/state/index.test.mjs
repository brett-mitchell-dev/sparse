/* eslint-disable max-len */
import { expect } from 'chai';

import {
  derive,
  Derived,
  joinDeferred,
  joinImmediate,
  signal,
  Signal,
} from './index.mjs';

describe ('@sprs/state', function () {
  describe ('class Signal', function () {
    describe ('Instantiating a signal with initial state', function () {
      it ('should use the given initial state', function () {
        const instance = new Signal (123);
        expect (instance.value).to.equal (123);
      });
    });

    context ('Instantiating a signal without initial state', function () {
      it ('should use undefined as the initial state', function () {
        const instance = new Signal ();
        expect (instance.value).to.be.undefined;
      });
    });

    describe ("Using '.set' to update the current value", function () {
      it ('should update the current value', function () {
        const s = new Signal ('initial');
        s.set ('updated');
        expect (s.value).to.equal ('updated');
      });
    });

    describe ("Using '.mut' to mutate the current value", function () {
      it ('should call the function with the previous value and set the new value to the result', function () {
        const s = new Signal ('initial');

        let calledWith = null;
        s.mut (prev => {
          calledWith = prev;
          return prev + '-updated';
        });

        expect (calledWith).to.equal ('initial');
        expect (s.value).to.equal ('initial-updated');
      });
    });

    describe ("Using '.get' to retrieve the current value", function () {
      it ('should return the current value', function () {
        const s = new Signal ('a');
        expect (s.get ()).to.equal ('a');

        s.set ('b');
        expect (s.get ()).to.equal ('b');
      });
    });

    describe ("Assigning a function to the 'value' event and then setting the value", function () {
      it ('should call the callback with the new value and some metadata', function () {
        const s = new Signal ('initial');

        let result = null;
        s.on ('value', (...value) => { result = value; });

        s.set ('updated');

        expect (result).to.deep.equal (['updated', { previous: 'initial' }]);
      });
    });

    describe ("Registering the same function to the 'value' event twice and then setting the value", function () {
      it ('should only call the callback once', function () {
        let called = 0;
        const callback = () => { called += 1; };

        const s = new Signal ('initial');
        s.on ('value', callback);
        s.on ('value', callback);

        s.set ('updated');

        expect (called).to.equal (1);
      });
    });

    describe ('Removing a callback function by reference and then setting the value', function () {
      it ('should not call the callback function', function () {
        let called = 0;
        const callback = () => { called += 1; };

        const s = new Signal ('a');
        s.on ('value', callback);
        s.set ('b');

        expect (called).to.equal (1);

        s.off ('value', callback);
        s.set ('c');
        
        expect (called).to.equal (1);
      }); 
    });

    describe ("Deriving one signal from another with '.derive'", function () {
      it ('should return a valid signal', function () {
        const s1 = new Signal ('/some/file/path');
        const s2 = s1.derive (
          v => v.split ('/').filter (s => s.length),
        );

        expect (s2).to.be.instanceof (Derived);
        expect (s2.get ()).to.deep.equal (['some', 'file', 'path']);

        s1.set ('/another/path/to/a/file.txt');

        expect (s2.get ()).to.deep.equal (['another', 'path', 'to', 'a', 'file.txt']);
      });
    });
  });

  describe ('class Derived', function () {
    describe ('Instantiating a Derived instance', function () {
      it ('should require correct parameters', function () {
        expect (() => {
          // @ts-ignore
          return new Derived ('not derivable');
        }).to.throw (
          "Parameter 'source' must support event listeners (function 'on' or 'addEventListener'), got not derivable",
        );
      });

      it ('should permit but not require an explicit initial value as the second parameter', function () {
        expect (() => new Derived (new Signal (123), { value: (x) => x })).to.not.throw ();
        expect (() => new Derived (new Signal (123), 321, { value: (x) => x })).to.not.throw ();
      });

      it ('should require that a handler map be given as the last parameter', function () {
        expect (() => {
          // @ts-ignore
          return new Derived (new Signal (123));
        }).to.throw (
          'Derived on a Signal or Derived accepts an optional initial value and a required value handler',
        );

        expect (() => {
          // @ts-ignore
          return new Derived (new Signal (123), 123);
        }).to.throw (
          "Deriving a Signal or Derived requires a handler for the 'value' event",
        );
      });

      context ('When given an instance of Signal or Derived', function () {
        it ('should allow the handler map to be expressed as a single function', function () {
          expect (() => new Derived (new Signal (123), num => num * 2)).to.not.throw ();
          expect (() => new Derived (new Signal (123), { value: num => num * 2 })).to.not.throw ();
        });
      });

      context ('When given an instance of Emitter, XForm, or EventTarget', function () {
        it ('should require an object-form handler map', function () {
          expect (() => new Derived (
            new EventTarget (),
            'initial_value',
            // @ts-ignore
            num => num * 2,
          )).to.throw ('Derived on an Emitter, EventTarget, or XForm requires an initial value and a handler map');

          expect (() => new Derived (
            new EventTarget (),
            'initial_value',
            { my_event: event => event.stopPropagation () },
          )).to.not.throw ();
        });
      });
    });

    describe ("Using '.get' to retrieve the current value", function () {
      it ('should return the current value', function () {
        const s = new Signal ('a');
        const d = new Derived (s, v => `Derived - ${v}`);
        expect (d.get ()).to.equal ('Derived - a');

        s.set ('b');
        expect (d.get ()).to.equal ('Derived - b');
      });
    });

    describe ("Assigning a function to the 'value' event and then setting the value", function () {
      it ('should call the callback with the new value and some metadata', function () {
        const s = new Signal ('initial');
        const d = new Derived (s, v => `Derived - ${v}`);

        let result = null;
        d.on ('value', (...value) => { result = value; });

        s.set ('updated');

        expect (result).to.deep.equal (['Derived - updated', { previous: 'Derived - initial' }]);
      });
    });

    describe ("Registering the same function to the 'value' event twice and then setting the value", function () {
      it ('should only call the callback once', function () {
        let called = 0;
        const callback = () => { called += 1; };

        const s = new Signal ('initial');
        const d = new Derived (s, value => `Derived - ${value}`);
        d.on ('value', callback);
        d.on ('value', callback);

        s.set ('updated');

        expect (called).to.equal (1);
      });
    });

    describe ('Removing a callback function by reference and then setting the value', function () {
      it ('should not call the callback function', function () {
        let called = 0;
        const callback = () => { called += 1; };

        const s = new Signal ('a');
        const d = new Derived (s, value => `Derived - ${value}`);
        d.on ('value', callback);
        s.set ('b');

        expect (called).to.equal (1);

        d.off ('value', callback);
        s.set ('c');
        
        expect (called).to.equal (1);
      }); 
    });

    describe ("Deriving one derived signal from another with '.derive'", function () {
      it ('should return a valid signal', function () {
        const s1 = new Signal ('/some/file/path');
        const d1 = new Derived (s1, value => `/derived${value}`);
        const d2 = d1.derive (
          v => v.split ('/').filter (s => s.length),
        );

        expect (d2).to.be.instanceof (Derived);
        expect (d2.get ()).to.deep.equal (['derived', 'some', 'file', 'path']);

        s1.set ('/another/path/to/a/file.txt');

        expect (d2.get ()).to.deep.equal (['derived', 'another', 'path', 'to', 'a', 'file.txt']);
      });
    });
  });

  describe ('function signal', function () {
    context ('When called', function () {
      it ('should return an instance of Signal with an appropriate initial value', function () {
        const s = signal (123);
        expect (s).to.be.instanceof (Signal);
        expect (s.value).to.equal (123);
      });
    });
  });

  describe ('function derive', function () {
    context ('When called', function () {
      it ('should return an instance of Derived with an appropriate initial value', function () {
        const s = signal ('value');
        const d = derive (s, v => `Derived - ${v}`);
        expect (d).to.be.instanceof (Derived);
        expect (d.value).to.equal ('Derived - value');
      });
    });
  });

  describe ('function joinImmediate', function () {
    context ('When called with a list of signals and derived values', function () {
      it ('should return an instance of Derived with an initial value containing a list with the initial values of the source list', function () {
        const s1 = signal (123);
        const s2 = signal ('abc');
        const d1 = derive (s1, v => v * 2);
        const d2 = s2.derive (v => v + ' - ' + [...v].reverse ().join (''));

        const joined = joinImmediate ([s1, d1, s2, d2]);
        expect (joined).to.be.instanceof (Derived);
        expect (joined.value).to.deep.equal ([
          123,
          246,
          'abc',
          'abc - cba',
        ]);
      });

      it ('should return a derived value that updates immediately every time one of the source items updates', function () {
        let called = 0;
        const callback = () => { called += 1; };

        const s1 = signal (123);
        const s2 = signal ('abc');
        const d1 = derive (s1, v => v * 2);
        const d2 = s2.derive (v => v + ' - ' + [...v].reverse ().join (''));

        const joined = joinImmediate ([s1, d1, s2, d2]);
        joined.on ('value', callback);

        s1.set (234);
        s2.set ('new value');

        expect (called).to.equal (4);
      });
    });
  });

  describe ('function joinDeferred', function () {
    context ('When called with a list of signals and derived values', function () {
      it (
        'should return an instance of Derived with an initial value containing a list with the initial values of the source list',
        function () {
          const s1 = signal (123);
          const s2 = signal ('abc');
          const d1 = derive (s1, v => v * 2);
          const d2 = s2.derive (v => v + ' - ' + [...v].reverse ().join (''));

          const joined = joinDeferred ([s1, d1, s2, d2]);
          expect (joined).to.be.instanceof (Derived);
          expect (joined.value).to.deep.equal ([
            123,
            246,
            'abc',
            'abc - cba',
          ]);
        });

      it (
        'should return a derived value that updates only one time for each update that occurs during the current event loop iteration',
        function (done) {
          let called = 0;
          let calledWith = null;
          const callback = (...args) => {
            called += 1;
            calledWith = args; 
          };

          const s1 = signal (123);
          const s2 = signal ('abc');
          const d1 = derive (s1, v => v * 2);
          const d2 = s2.derive (v => v + ' - ' + [...v].reverse ().join (''));

          const joined = joinDeferred ([s1, d1, s2, d2]);
          joined.on ('value', callback);

          s1.set (234);
          s2.set ('new value');

          expect (called).to.equal (0);
          expect (calledWith).to.be.null;

          queueMicrotask (() => {
            expect (called).to.equal (1);
            expect (calledWith).to.deep.equal ([
              [
                234,
                468,
                'new value',
                'new value - eulav wen',
              ],

              {
                previous: [
                  123,
                  246,
                  'abc',
                  'abc - cba',
                ],
              },
            ]);
            done ();
          });
        });
    });
  });
});
