
import { Emitter, XForm } from '@sprs/emitter';

export declare class Signal <T> {
  emitter: Emitter <['value']>;
  value: T;
  constructor (init?: T);
  get(): T;
  set(newValue: T): void;
  mut(xform: (currentValue: T) => T | Promise <T>): void;
  on(event: 'value', listener: (value: T, metadata: { previous: T, [key: string]: any }) => any): void;
  off(event: 'value', listener: any): void;
  derive <R> (deriver: (value: T) => R): Derived <Signal <T>, R>;
}

export function signal <T> (init?: T): Signal <T>;

type Deriver <R, Source extends Derivable> = (
  Source extends Signal <infer Inner>
    ? ((value: Inner) => R) | { value: (value: Inner) => R }
    : (
  Source extends Derived <infer Inner, infer InnerSource>
    ? ((value: Inner) => R) | { value: (value: Inner) => R }
    : (
  Source extends EventTarget
    ? { [key: string]: (event: Event) => R }
    : (
  Source extends Emitter <infer Inner>
    ? { [K in Source['events'][number]]?: (value: any) => R }
    : never
  )
  )
  )
)

type InnerOf <Source> = (
  Source extends Signal <infer Inner>
    ? Inner
    : (
  Source extends Derived <infer Inner, infer InnerSourceInner, infer InnerSource>
    ? Inner
    : never
  )
)

type Derivable = (
  | Signal <any>
  | Derived <any, any>
  | XForm <any, any, any>
  | Emitter <any> 
  | EventTarget
)

export declare class Derived <Source extends Derivable, T> {
  emitter: Emitter <['value']>;
  value: T;

  constructor (source: Source, derivers: Deriver <T, Source>);
  constructor (source: Source, init: InnerOf <Source>, derivers: Deriver <T, Source>);

  get(): T;
  on(event: 'value', listener: (value: T, metadata: { previous: T, [key: string]: any }) => any): void;
  off(event: 'value', listener: any): void;
  derive <R> (deriver: (value: T) => R): Derived <Derived <Source, T>, R>;
}

export function derive
  <Source extends Signal <any> | Derived <any, any>, R>
  (source: Source, derivers: Deriver <R, Source>)
    : Derived <Source, R>;
export function derive
  <Source extends Derivable, R>
  (source: Source, init: R, derivers: Derivers <R, Source>)
    : Derived <Source, R>;

/**
 * This function takes a list of signals and joins them into a
 * single signal.
 *
 * The resulting signal's value is an array whose elements are the
 * values of the source signals. These values are maintained in the
 * order that the sources were given.
 *
 * The resulting signal will be updated immediately each time a source
 * is updated.
 *
 * To defer updates and get one batched update to the joined signal,
 * use `joinDeferred` (or `joind`) instead.
 *
 * Example:
 *
 * ```js
 * const source1 = signal(1);
 * const source2 = signal(2);
 * const source3 = signal(3);
 *
 * const joined = joinImmediate([source1, source2, source3]);
 * joined.on('value', console.log);
 *
 * source1.set('updated');
 * // -> 'joined' listener logs "['updated', 2, 3]"
 * source2.set('updated');
 * // -> 'joined' listener logs "['updated', 'updated', 3]"
 * ```
 */
export function joinImmediate
  <Derivables extends (Signal <any> | Derived <any, any>)[]>
  (derivables: Derivables)
  : Derived <any, {
    [K in keyof Derivables]: (
      Derivables[K] extends Signal <infer Inner> | Derived <any, infer Inner>
        ? Inner
        : never
    )
  }>

/**
 * This function takes a list of signals and joins them into a
 * single signal.
 *
 * The resulting signal's value is an array whose elements are the
 * values of the source signals. These values are maintained in the
 * order that the sources were given.
 *
 * The resulting signal will be updated immediately each time a source
 * is updated.
 *
 * To defer updates and get one batched update to the joined signal,
 * use `joind` (or `joinDeferred`) instead.
 *
 * Example:
 *
 * ```js
 * const source1 = signal(1);
 * const source2 = signal(2);
 * const source3 = signal(3);
 *
 * const joined = joini([source1, source2, source3]);
 * joined.on('value', console.log);
 *
 * source1.set('updated');
 * // -> 'joined' listener logs "['updated', 2, 3]"
 * source2.set('updated');
 * // -> 'joined' listener logs "['updated', 'updated', 3]"
 * ```
 */
export function joini
  <Derivables extends (Signal <any> | Derived <any, any>)[]>
  (derivables: Derivables)
  : Derived <any, {
    [K in keyof Derivables]: (
      Derivables[K] extends Signal <infer Inner> | Derived <any, infer Inner>
        ? Inner
        : never
    )
  }>

/**
 * This function takes a list of signals and joins them into a
 * single signal.
 *
 * The resulting signal's value is an array whose elements are the
 * values of the source signals. These values are maintained in the
 * order that the sources were given.
 *
 * The resulting signal will NOT be updated immediately each time a
 * source signal is updated; rather, it registers a microtask to
 * update its value at the end of the current event loop iteration.
 *
 * This means that all updates to source signals will be batched into
 * a single update, even repeated updates to the same source signal.
 *
 * To immediately update the joined signal with each update to each
 * source, use `joinImmediate` (or `joini`) instead.
 *
 * Example:
 *
 * ```js
 * const source1 = signal(1);
 * const source2 = signal(2);
 * const source3 = signal(3);
 *
 * const joined = joinDeferred([source1, source2, source3]);
 * joined.on('value', console.log);
 *
 * source1.set('updated');
 * // -> No change
 * source2.set('updated');
 * // ... Some more work ...
 *
 * // -> 'joined' listener logs "['updated', 'updated', 3]"
 * ```
 */
export function joinDeferred
  <Derivables extends (Signal <any> | Derived <any, any>)[]>
  (derivables: Derivables)
  : Derived <any, {
    [K in keyof Derivables]: (
      Derivables[K] extends Signal <infer Inner> | Derived <any, infer Inner>
        ? Inner
        : never
    )
  }>

/**
 * This function takes a list of signals and joins them into a
 * single signal.
 *
 * The resulting signal's value is an array whose elements are the
 * values of the source signals. These values are maintained in the
 * order that the sources were given.
 *
 * The resulting signal will NOT be updated immediately each time a
 * source signal is updated; rather, it registers a microtask to
 * update its value at the end of the current event loop iteration.
 *
 * This means that all updates to source signals will be batched into
 * a single update, even repeated updates to the same source signal.
 *
 * To immediately update the joined signal with each update to each
 * source, use `joini` (or `joinImmediate`) instead.
 *
 * Example:
 *
 * ```js
 * const source1 = signal(1);
 * const source2 = signal(2);
 * const source3 = signal(3);
 *
 * const joined = joind([source1, source2, source3]);
 * joined.on('value', console.log);
 *
 * source1.set('updated');
 * // -> No change
 * source2.set('updated');
 * // ... Some more work ...
 *
 * // -> 'joined' listener logs "['updated', 'updated', 3]"
 * ```
 */
export function joind
  <Derivables extends (Signal <any> | Derived <any, any>)[]>
  (derivables: Derivables)
  : Derived <any, {
    [K in keyof Derivables]: (
      Derivables[K] extends Signal <infer Inner> | Derived <any, infer Inner>
        ? Inner
        : never
    )
  }>
