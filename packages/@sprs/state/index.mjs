
import { Emitter, XForm } from '@sprs/emitter';

const assert = (cond, msg) => { if (!cond) throw new Error (msg); };

export class Signal {

  constructor (init) {
    this.emitter = new Emitter (['value']);
    this.value = init;
  }

  on (event, listener) { 
    return this.emitter.on (event, listener);
  }

  off (event, listener) { 
    return this.emitter.off (event, listener);
  }

  get () {
    return this.value;
  }

  set (newValue, metadata = {}) {
    metadata.previous = this.value;
    this.value = newValue;
    this.emitter.emit ('value', newValue, metadata);
  }

  mut (fn, metadata = {}) {
    assert (typeof fn === 'function', 'Signal.mut requires a function');
    const newVal = fn (this.value);
    return this.set (newVal, metadata);
  }

  derive (fn) {
    return new Derived (this, this.value, fn);
  }

}

const derivedHandleSignalValue = 
  (derived) => function () {
    const newValue = derived.handlers.value.apply (derived, arguments);
    const metadata = typeof arguments[1] === 'object' && arguments[1] !== null
      ? arguments[1]
      : {};
    metadata.previous = derived.value;
    derived.value = newValue;
    derived.emitter.emit ('value', newValue, metadata);
  };

const derivedHandleEvent = 
  (event, derived) => function () {
    const newValue = derived.handlers[event].apply (derived, arguments);
    derived.value = newValue;
    derived.emitter.emit ('value', newValue);
  };

export class Derived {

  constructor (source, init, handlers) {
    assert (
      source && (typeof source.on === 'function' || typeof source.addEventListener === 'function'),
      `Parameter 'source' must support event listeners (function 'on' or 'addEventListener'), got ${source}`,
    );

    this.emitter = new Emitter (['value']);

    if (source instanceof Signal || source instanceof Derived) {
      assert (
        typeof init !== 'undefined',
        'Derived on a Signal or Derived accepts an optional initial value and a required value handler',
      );

      // No explicit initial value
      if (typeof handlers === 'undefined') {
        // Interpret value passed to 'init' as the handler
        handlers = init;
      }

      // Normalize to event map form
      if (typeof handlers === 'function') {
        handlers = { value: handlers };
      }

      assert (
        handlers && typeof handlers.value === 'function',
        "Deriving a Signal or Derived requires a handler for the 'value' event",
      );

      // Get initial value
      this.value = handlers.value (source.get ());
      this.handlers = handlers;
      source.on ('value', derivedHandleSignalValue (this));
    }

    else {
      assert (
        typeof handlers === 'object' && handlers !== null,
        'Derived on an Emitter, EventTarget, or XForm requires an initial value and a handler map',
      );

      this.handlers = handlers;
      this.value = init;
      const listenEvents = Object.keys (handlers);
      let i = 0, len = listenEvents.length;

      if (
        source instanceof XForm
        || source instanceof Emitter
      ) {
        while (i < len) {
          const evt = listenEvents[i];
          source.on (evt, derivedHandleEvent (evt, this, false));
          i++;
        }
      }

      else if (source instanceof EventTarget) {
        while (i < len) {
          const evt = listenEvents[i];
          source.addEventListener (evt, derivedHandleEvent (evt, this, false));
          i++;
        }
      }
    }
  }

  on (event, listener) { 
    return this.emitter.on (event, listener);
  }

  off (event, listener) { 
    return this.emitter.off (event, listener);
  }

  get () {
    return this.value;
  }

  derive (fn) {
    return new Derived (this, this.value, fn);
  }

}

export const signal = (init) => {
  return new Signal (init);
};

export const derive = (source, init, derivers) => {
  return new Derived (source, init, derivers);
};

/** @param {(Signal | Derived)[]} sources */
export const joinImmediate = (sources) => {
  const joinedSignal = new Signal (sources.map (source => source.value));
  const joinedDerived = new Derived (joinedSignal, v => v);

  let i = 0, len = sources.length;
  while (i < len) {
    sources[i].on ('value', value => {
      joinedSignal.mut (prev => {
        prev[i] = value;
        return prev;
      });
    });
    i++;
  }

  return joinedDerived;
};

export const joini = joinImmediate;

/** @param {(Signal | Derived)[]} sources */
export const joinDeferred = (sources) => {
  const joinedSignal = new Signal (sources.map (source => source.value));
  const joinedDerived = new Derived (joinedSignal, v => v);
  const updateCache = [];

  const updateJoinedValue = () => {
    joinedSignal.mut (prev => {
      const newValue = [...prev];
      let j = 0, len = updateCache.length;
      while (j < len) {
        newValue[updateCache[j][0]] = updateCache[j][1];
        j++;
      }
      updateCache.length = 0;
      return newValue;
    });
  };

  let i = 0, len = sources.length;
  while (i < len) {
    const _i = i;
    sources[i].on ('value', value => {
      /*
       * If this is the first update to the joined item this event loop
       * iteration, enqueue a microtask to update the joined item when
       * the current iteration exits, but before the next one starts.
       */
      if (updateCache.length === 0) {
        queueMicrotask (updateJoinedValue);
      }
      updateCache.push ([_i, value]);
    });
    i++;
  }

  return joinedDerived;
};

export const joind = joinDeferred;
