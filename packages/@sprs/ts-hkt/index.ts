
// Beware the (Spirit Demon)[https://grugbrain.dev/#grug-on-complexity] that is HKT in TS

// This has been simplified and improved from a far more horrendous beast
// which can be found in the next circle of hell:
// https://gitlab.com/brett-mitchell-dev/miscellany/types

type CoerceArray <T> = T extends any[] ? T : never;
type PartialTuple <T extends any[]> = CoerceArray <{ [K in keyof T]?: T[K] }>;
type RelaxedTuple <T extends any[]> = CoerceArray <{ [K in keyof T]: any }>;
type DiffTuples <T1 extends any[], T2 extends any[]> = (
  T2 extends [...RelaxedTuple <T1>, ...infer Rest] ? Rest : never
);

export interface TypeConstructor <
  ParamConstraint extends any[] = any[],
  ReturnConstraint = unknown,
> {
  constraint: ParamConstraint;
  args: this['apply'] extends this['constraint']
    ? this['apply'] : this['constraint'];
  apply: unknown;
  result: ReturnConstraint;
}

export type apply <
  Ctor extends TypeConstructor <any[]>,
  Args extends Ctor['constraint'],
> = (Ctor & { apply: Args })['result'];

export interface partial <
  Ctor extends TypeConstructor <any[]>,
  Args extends PartialTuple <Ctor['constraint']>,
> extends TypeConstructor <DiffTuples <Args, Ctor['constraint']>> {
  result: apply <Ctor, [...Args, ...this['args']]>;
  test: DiffTuples <Args, Ctor['constraint']>
}

export type call <
  Ctor extends TypeConstructor,
  Args extends PartialTuple <Ctor['constraint']>,
> = (
  Args['length'] extends Ctor['constraint']['length']
    ? apply <Ctor, Args>
    : partial <Ctor, Args>
);

export type tc <P extends any[] = any[], R = unknown> = TypeConstructor <P, R>;
