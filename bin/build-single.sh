#!/usr/bin/env bash

function get_output_name {
  node -p 'require("./package.json").rollup.output.globals'
}

# @sprs packages declare their rollup global name in their package.json
# This function finds dependencies between @sprs packages and assigns
# a global mapping for rollup to use when generating a UMD bundle.
#
# As of the writing of this function, @sprs packages only depend on
# each other at runtime. If that changes in the future, rollup may need
# extra configuration, or those packages may need to be excluded from
# the UMD build step.
function get_globals {
node - << EOF
  const path = require('path');
  const pkg = require(path.resolve(process.cwd(), 'package.json'));
  const globals = [];
  Object.entries(pkg?.dependencies || {}).forEach(([k, v]) => {
    if (k.startsWith('@sprs')) {
      const depPkg = require(k + '/package.json');
      const rollupGlobal = depPkg?.rollup?.output?.globals;
      if (rollupGlobal) {
        globals.push(k + ':' + rollupGlobal);
      }
    }
  });
  console.log(globals.join(','));
EOF
}

function get_externals {
node - << EOF
  const path = require('path');
  const pkg = require(path.resolve(process.cwd(), 'package.json'));
  console.log(Object.keys(pkg.dependencies || {}).join(','));
EOF
}

function main {
  local script_dir=`dirname "$0"`
  local pkg_dir="$1"

  pushd "$pkg_dir" > /dev/null

  npx rollup \
    ./index.mjs \
    --file dist/index.esm.mjs \
    --format esm \
    --external `get_externals`

  npx rollup \
    ./index.mjs \
    --file dist/index.esm.min.mjs \
    --format esm \
    --external `get_externals` \
    --config "$script_dir"/../rollup.min.config.mjs

  npx rollup \
    ./index.mjs \
    --file dist/index.umd.js \
    --format umd \
    --name `get_output_name` \
    --globals `get_globals`

  npx rollup \
    ./index.mjs \
    --file dist/index.umd.min.js \
    --format umd \
    --name `get_output_name` \
    --globals `get_globals` \
    --config "$script_dir"/../rollup.min.config.mjs

  [ -f ./index.d.mts ] \
    && printf '\n%s\n' './index.d.mts -> ./dist/index.d.ts' \
    && cp ./index.d.mts ./dist/index.d.ts \
    && printf '%s\n' 'Copied'

  popd > /dev/null
}

main "$@"

