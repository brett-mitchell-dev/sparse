#!/usr/bin/env bash

function get_pkg_prop {
  local pkg="$1"
  local prop="$2"
  grep '"'"$prop"'":' < "$pkg" | sed 's/[ \t]*"'"$prop"'": "\(.*\)",/\1/'
}

function main {
  printf '\n%s\n\n' 'Begin deploy'

  if [ -z "$CI_NPM_ACCESS_TOKEN" ] ; then
    printf '%s\n\n' 'No NPM access token available'
    exit 1
  fi

  for pkg in ./packages/@sprs/*/package.json ; do
    printf '%s\n' "Checking deploy status: $pkg"
    local name=`get_pkg_prop "$pkg" name`
    local version=`get_pkg_prop "$pkg" version`
    local latestPublishedVersion=`pnpm view "$name" version 2> /dev/null`
    printf '%s\n' "Local version     : $version"
    printf '%s\n' "Published version : $latestPublishedVersion"
    if [ "$version" != "$latestPublishedVersion" ] ; then
      printf '%s\n' "Deploying package $name"
      cd `dirname $pkg`
      pnpm publish --access=public
      cd -
    else
      printf '%s\n' "Not deploying $name"
    fi
    printf '\n'
  done

  printf '%s\n' 'Done'
}

main "$@"
