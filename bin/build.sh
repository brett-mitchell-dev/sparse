#!/usr/bin/env bash

function main {
  local build_variety="$1"

  printf '%s\n' "  Begin build$build_variety"

  for pkg in ./packages/@sprs/*/package.json ; do
    if [[ ! -z `node -p "require('$pkg').scripts['build$build_variety'] || ''"` ]] ; then
      printf '\n%s\n' '  Building '`dirname "$pkg"`
      (cd `dirname "$pkg"` && npm run "build$build_variety")
    fi
  done
}

main "$@"

